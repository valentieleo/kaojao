//
//  AddressRegisterViewController.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 10/23/16.
//  Copyright © 2016 Tana Chaijamorn. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire

class AddressRegisterViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    var userCode:String?
    var StoreCode:String?
    let picker = UIImagePickerController()
    
    @IBOutlet weak var ImageProfile: UIImageView!
    
    let reachability = Reachability()
    
    @IBOutlet weak var addressOneLabel: UILabel!
    @IBOutlet weak var addressOneTextfield: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        picker.delegate = self
        self.setNavigationbarToTranspalent()
        self.title = NSLocalizedString("signUp", comment: "signUp")
        
        self.addressOneLabel.text = "ตั้งชื่อเพจ Kaojao ของคุณ"
        self.addressOneTextfield.placeholder = "กรุณากรอก Address"
        
        self.nextButton.setTitle(NSLocalizedString("next", comment: "next"), for: UIControlState.normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func nextFunc(_ sender: AnyObject) {
        if Reachability.isConnectedToNetwork() == true {
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = MBProgressHUDMode.indeterminate
            loadingNotification.label.text = "Loading"
            let address = self.addressOneTextfield.text ?? ""
            
            let api = ConnectAPI<ErrorReponse>()
            
            api.storeDetail(userCode!, storeid: StoreCode!, completionHandler: { (response, error) in
                
              api.updateStore(self.userCode!, idStore: self.StoreCode!, addressLinkStore: address, nameStore: "", dataStore: response!, descriptionStore: "", phoneStore: "", contactInfo1: "", contactInfo2: "", contactInfo3: "", completionHandler: { (responsIn, errorIn) in
                    ConnectAPI<LinkResponse>.createLinkforUploadImageLogo(self.StoreCode!, token: self.userCode!, completionHandler: { resObj, error in
                        let imageData = UIImageJPEGRepresentation(self.ImageProfile.image!, 75)
                        //                    let imageData2 = UIImagePNGRepresentation(self.ImageProfile.image!)
                        let urlString = resObj?.URLItem
                        Alamofire.upload(imageData!, to: urlString!, method: HTTPMethod.put, headers: ["x-ms-blob-type": "BlockBlob"]).response { response in
                            if response.response?.statusCode == 201 {
                                MBProgressHUD.hide(for: self.view, animated: true)
                                UserDefaults.standard.set(imageData, forKey: "LogoStore")
                                self.performSegue(withIdentifier: "toLastPageRegister", sender: nil)
                            }else{
                                VTAlertView().type(.alert).title("Error").message("Can not upload image").button("dismiss", action: {
                                    
                                }).show(self)
                            }
                        }
                    })
                })
                
            })
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            VTAlertView()
                .title("No Internet Connection")
                .message("Make sure your device is connected to the internet.")
                .type(.alert)
                .button("OK", action: {
                    
                })
                .show(self)
        }
    }

    func setNavigationbarToTranspalent() {
        self.navigationController?.isNavigationBarHidden = false
        
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        
        if (bar == nil) {
            
        }else{
            bar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            bar.shadowImage = UIImage()
//            bar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
          bar.backgroundColor = UIColor(hex: "#37414d")
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toLastPageRegister" {
            if let NameContactVC = segue.destination as? NameAndContactViewController {
                NameContactVC.userCode = self.userCode
                NameContactVC.StoreCode = self.StoreCode
                NameContactVC.addressCode = self.addressOneTextfield.text!
            }
        }
    }
    @IBAction func selectImageforLogo(_ sender: Any) {
        VTAlertView()
            .title("Select Image")
            .type(.actionSheet)
            .button("Camera", action: {
                self.callImageController(mode: "camera")
            })
            .button("Library", action: {
                self.callImageController(mode: "library")
            })
            .show(self)
    }

    func callImageController(mode: String) {
        picker.allowsEditing = false
        
        if mode == "camera" {
            picker.sourceType = .camera
            picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
        }else{
            picker.sourceType = .photoLibrary
            picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        }
        
        present(picker, animated: true, completion: nil)
    }
    
    //MARK: - ImagePicker Delegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        ImageProfile.contentMode = .scaleAspectFit
        ImageProfile.image = chosenImage
        dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
