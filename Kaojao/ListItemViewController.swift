//
//  ListItemViewController.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 3/6/2560 BE.
//  Copyright © 2560 Tana Chaijamorn. All rights reserved.
//

import UIKit
import MBProgressHUD
import Kingfisher
import SDWebImage
import FBSDKCoreKit
import FBSDKLoginKit

class ListItemViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
  
  var menulist = [Any]()
  var pages : Int = 1
  @IBOutlet weak var manuListCollection: UICollectionView!
  private let reuseIdentifier = "Cell"
  let user : String = (UserDefaults.standard.value(forKey: "UserToken") as? String)!
  @IBOutlet weak var heightConstraint: NSLayoutConstraint!
  
  @IBOutlet weak var addItemButton: UIButton!
  var type:String?
  var index : Int = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.navigationItem.setHidesBackButton(false, animated:true);
    self.setNavigationbarToTranspalent()
    
    let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
    loadingNotification.mode = MBProgressHUDMode.indeterminate
    loadingNotification.label.text = "Loading"
    self.heightConstraint.constant = 0
    self.addItemButton.isHidden = true
    
    if type == "order" {
      self.title = NSLocalizedString("Orders", comment: "Orders")
      kaojaoProvider.request(.loadOrder(page: self.pages), completion: { result in
        switch result {
        case let .success(response):
          do {
            
            let collection = try response.mapObject(ListDataResponse.self)
            
            self.menulist = collection.collection
            
            MBProgressHUD.hide(for: self.view, animated: true)
            self.manuListCollection.reloadData()
          } catch {
            
          }
        case .failure(_):
          self.setAlert(AlertStr: "API loadData Error")
          MBProgressHUD.hide(for: self.view, animated: true)
        }
      })
    }else if (type == "store") {
      self.title = NSLocalizedString("store", comment: "store")
      
      kaojaoProvider.request(.loadAllStore, completion: { result in
        switch result {
        case let .success(response):
          do {
            
            let collection = try response.mapArray(ListDataResponse.self)
            
            self.menulist = collection
            
            MBProgressHUD.hide(for: self.view, animated: true)
            self.manuListCollection.reloadData()
          } catch {
            
          }
        case .failure(_):
          self.setAlert(AlertStr: "API loadData Error")
          MBProgressHUD.hide(for: self.view, animated: true)
        }
      })
      
    } else {
      self.title = NSLocalizedString("items", comment: "items")
      self.heightConstraint.constant = 80
      self.addItemButton.isHidden = false
      self.addItemButton.setTitle(NSLocalizedString("newItem", comment: "newItem"), for: UIControlState.normal)
      kaojaoProvider.request(.loadData(type: "Items", page: self.pages), completion: { result in
        switch result {
        case let .success(response):
          do {
            
            let collection = try response.mapObject(ListDataResponse.self)
            
            self.menulist = collection.collection
            
            MBProgressHUD.hide(for: self.view, animated: true)
            self.manuListCollection.reloadData()
          } catch {
            
          }
        case .failure(_):
          self.setAlert(AlertStr: "API loadData Error")
          MBProgressHUD.hide(for: self.view, animated: true)
        }
      })
      
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.menulist.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    if type == "store" {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! ItemListCollectionViewCell
      let object  = self.menulist[indexPath.row] as? ListDataResponse
      
      let name : String = object!.address!
      cell.nameLabel.text = "@"+name
      cell.nameLabel.textAlignment = .center
      let urlString = object!.logo ?? ""
      
      let url = URL(string: urlString)
      
      cell.imageView.sd_setImage(with: url, placeholderImage: Image(named: "ImageDefault"))
      
      cell.imageView.backgroundColor = Color.clear
      cell.priceLabel.text = nil
      cell.priceLabel.isHidden = true
      cell.listButton.isHidden = false
      cell.listButton.setCornerRadius(radius: 15.0)
      cell.listButton.tag = object!.id!
      cell.listButton.addTarget(self, action: #selector(pushwithAnimation(animated:)), for: .touchUpInside)
      return cell
    }else{
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! ItemListCollectionViewCell
      
      let object  = self.menulist[indexPath.row] as? ItemResponse
      cell.nameLabel.text = object?.name
      
      let userId = object?.userId
      let itemId = object?.id
      
      cell.imageView.image = Image(named: "ImageDefault")
      
      if userId == "" || itemId == 0 {
        cell.imageView.image = Image(named: "noImg")
      }else{
        
        
        let urlString = "https://acconx.blob.core.windows.net/\(userId!)/items/\(itemId!)/290/1.jpg"
        //                let urlString = "https://acconx.blob.core.windows.net/\(userId!)/items/\(itemId!)/1.jpg"
        let url = URL(string: urlString)
        cell.imageView.sd_setImage(with: url, placeholderImage: Image(named: "noImg"))
        
      }
      
      
      let price = String(format: "%.2f ", (object?.sellPrice)!)
      let currencyTitle = UserDefaults.standard.value(forKey: "Currency") as? String
      cell.priceLabel.text = price + currencyTitle!
      return cell
    }
    
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if type == "store" {
      let ListItemStore = self.storyboard?.instantiateViewController(withIdentifier: "ListItemStoreView") as? ListItemStoreViewController
      
      let selectObject = self.menulist[indexPath.row] as! ListDataResponse
      ListItemStore?.storeDetail = selectObject
      self.navigationController?.pushViewController(ListItemStore!, animated: true)
    }
    
  }
  
  func collectionView(_ collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize {
    if type == "store" {
      let cellSize = CGSize(width: self.manuListCollection.frame.width-10, height: self.manuListCollection.frame.height/3)
      return cellSize
    } else {
      let cellSize = CGSize(width: self.manuListCollection.frame.width/2-10, height: self.manuListCollection.frame.width/2)
      return cellSize
    }
  }
  
  func setNavigationbarToTranspalent() {
    self.navigationController?.isNavigationBarHidden = false
    
    let bar:UINavigationBar! =  self.navigationController?.navigationBar
    
    if (bar == nil) {
      
    }else{
      bar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
      bar.shadowImage = UIImage()
      bar.backgroundColor = UIColor(hex: "#37414d")
    }
    
  }
  
  // MARK: - Facebook Delegate
  public func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
    debugPrint("User Logged In")
    if error != nil {
      debugPrint("Process error")
      MBProgressHUD.hide(for: self.view, animated: true)
    } else if result.isCancelled {
      debugPrint("Handle cancellations")
      MBProgressHUD.hide(for: self.view, animated: true)
    } else {
      if result.grantedPermissions.contains("email") {
        //                self.returnUserData()
      }
    }
  }
  
  // MARK: - Push View
  @objc func pushwithAnimation(animated: UIButton) {
    let btntag = animated.tag
    if (FBSDKAccessToken.current() != nil) {
      // User is already logged in, do work such as go to next view controller.
      self.gotolistScreen(storeID: btntag)
      
    } else {
      self.facebookLoginfunc(storeID: btntag)
    }
  }
  
  // Once the button is clicked, show the login dialog
  func facebookLoginfunc(storeID: Int) {
    let login = FBSDKLoginManager.init()
    let permission = ["public_profile", "email"]
    
    let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
    loadingNotification.mode = MBProgressHUDMode.indeterminate
    loadingNotification.label.text = "Loading"
    
    login.logIn(withReadPermissions: permission, from: self) { (result, error) in
      
      if ((error) != nil) {
        MBProgressHUD.hide(for: self.view, animated: true)
        debugPrint("Process error")
      } else if (result?.isCancelled)! {
        MBProgressHUD.hide(for: self.view, animated: true)
        debugPrint("Cancelled")
      } else {
        debugPrint("Log in")
        
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
          
          if ((error) != nil) {
            // Process error
            debugPrint("Error: \(String(describing: error))")
          } else {
            guard let result = result as? Dictionary<String, AnyObject> else {
              self.setAlert(AlertStr: "fb api error")
              return
            }
            
            let userEmail = result["email"] as? String
            UserDefaults.standard.set(FBSDKAccessToken.current().tokenString!, forKey: "FacebookToken")
            
            kaojaoProvider.request(.externalLogin(provider: "Facebook", email: userEmail!, accessToken: FBSDKAccessToken.current().tokenString), completion: { result in
              switch result {
              case let .success(response):
                MBProgressHUD.hide(for: self.view, animated: true)
                do {
                  let exlogin : ExternalLoginResponse? = try response.mapObject(ExternalLoginResponse.self)
                  
                  if let exlogin = exlogin {
                    let token_type = exlogin.token_type!
                    let access_token = exlogin.access_token!
                    let user : String = token_type+" "+access_token
                    UserDefaults.standard.set(user, forKey: "UserToken")
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.gotolistScreen(storeID: storeID)
                  }
                } catch {
                  
                }
              case .failure(_):
                self.setAlert(AlertStr: "API ExternalLogin Error")
                MBProgressHUD.hide(for: self.view, animated: true)
              }
            })
          }
          
        })
      }
    }
  }
  
  
  func gotolistScreen(storeID: Int) {
    ConnectAPI<ListStoreResponse>.getstorelist(FBSDKAccessToken.current().tokenString, completionHandler: { response,error in
      if error != nil {
        print("error")
      }else{
        if (response?.list.count)! > 0 {
          let PageFBVC = self.storyboard?.instantiateViewController(withIdentifier: "PageFBListView") as? PageFBListViewController
          PageFBVC?.listPage = (response?.list)!
          let storeIdString = storeID.toString 
          PageFBVC?.storeID =  storeIdString
          
          self.navigationController?.pushViewController(PageFBVC!, animated: true)
        }else{
          print("no page")
        }
        
      }
    })
  }
  
  public func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
    debugPrint("User Logged Out")
  }
  // MARK: - Set UI
  func setAlert(AlertStr:String){
    VTAlertView()
      .title("Kaojao")
      .message(AlertStr)
      .type(.alert)
      .button("Dismiss", action: {
        
      })
      .show(self)
    
    let alertController = UIAlertController(title: "Kaojao", message:
      AlertStr, preferredStyle: UIAlertControllerStyle.alert)
    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
    
    self.present(alertController, animated: true, completion: nil)
  }
  @IBAction func addNewItemFunc(_ sender: Any) {
    let AddVC = self.storyboard?.instantiateViewController(withIdentifier: "AddDataView") as? AddDataViewController
    self.navigationController?.pushViewController(AddVC!, animated: true)
  }
}
