//
//  NewConnectApi.swift
//  Kaojao
//
//  Created by MacBook on 5/19/2560 BE.
//  Copyright © 2560 Tana Chaijamorn. All rights reserved.
//

import Foundation
import Moya

//let kaojaoProvider = MoyaProvider<kaojaoService>()

let kaojaoProvider = MoyaProvider<kaojaoService>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])

enum kaojaoService {
    case login(username: String, password: String)
    case register(email: String, password: String)
    case externalLogin(provider: String, email: String, accessToken: String)
    case loadData(type: String, page: Int)
    case loadOrder(page: Int)
    case loadAllStore
    case loadItemInStore(storeName: String)
    case checkApi
    case createItem(nameItem: String, sellAmount: Float)
    case createLinkforUploadImage(IDItem: String, ItemCount: Int)
    case savePage(parameter:Dictionary<String, Any>)
    case createLinkforUploadImageLogo(storeId: String)
    case updateStoreInfo(object: ListDataResponse)
}

// MARK: - TargetType Protocol Implementation
extension kaojaoService: TargetType {
  
    var baseURL: URL { return URL(string: "https://api.kaojao.com")! }
    
    var path: String {
        switch self {
        case .login(_, _):
            return "/token"
        case .loadAllStore:
            return "/api/stores/list"
        case .register(_, _):
            return "/api/user/register"
        case .externalLogin(_, _, _):
            return "/api/User/ExternalLogin"
        case .loadData(_, _):
            return "/api/items/list"
        case .loadOrder(_):
            return "/api/orders/list"
        case .loadItemInStore(_):
            return "/api/items/items"
        case .checkApi:
            return "/api/incomes/list"
        case .createItem(_, _):
            return "/api/items/create"
        case .createLinkforUploadImage(_, _):
            return "/api/storage/getwritesas"
        case .savePage(_):
            return "/api/stores/connectFBPages"
        case .createLinkforUploadImageLogo(_):
            return "/api/storage/getwritesas"
        case .updateStoreInfo(_):
            return "/api/stores/update"
        }
    }
    
    var method: Moya.Method {
        switch self {
      case .loadData, .checkApi, .loadOrder, .loadAllStore, .loadItemInStore, .createLinkforUploadImage, .createLinkforUploadImageLogo:
            return .get
        case .login, .register, .externalLogin, .createItem, .savePage:
            return .post
        case .updateStoreInfo:
            return .put
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .login(let username, let password):
            return [
              "grant_type": "password",
              "username": username,
              "password": password
          ]
        case .register(let email, let password):
            return [
              "email": email,
              "password": password,
              "confirmPassword": password
          ]
        case .externalLogin(let provider, let email, let accessToken):
            return [
              "provider": provider,
              "email": email,
              "externalAccessToken": accessToken
          ]
        case .loadData(let type, let page):
          if type == "Items" {
            return [
              "pageIndex": "\(page)",
              "pageSize": "10",
              "reverse":"true",
              "sortBy": "createdDate"
            ]
          }else{
            return [
              "pageIndex": "\(page)",
              "pageSize": "10",
              "reverse":"true",
              "sortBy": "incomeDate"
            ]
          }
        case .loadOrder(let page):
          return [
            "pageIndex": "\(page)",
            "pageSize": "10",
            "reverse":"true",
            "sortBy": "createdDate"
          ]
        case .loadItemInStore(let pageName):
          return [
            "pageName": pageName,
            "pageIndex": "1",
            "pageSize": "20",
            "reverse":"true",
            "sortBy": "createdDate"
          ]
        case .updateStoreInfo(let object):
            let id = object.id?.toString ?? ""
            let address = object.address ?? ""
            let contactInfo1 = object.contactInfo1 ?? ""
            let contactInfo2 = object.contactInfo2 ?? ""
            let contactInfo3 = object.contactInfo3 ?? ""
            let description = object.description ?? ""
            let name = object.nameStore ?? ""
            let paymentInstructions = object.paymentInstructions ?? ""
            let phone = object.phone ?? ""
            let website = object.website ?? ""
            let welcomeMessage = object.welcomeMessage ?? ""
          return [
            "id": id,
            "address": address,
            "contactInfo1": contactInfo1,
            "contactInfo2": contactInfo2,
            "contactInfo3": contactInfo3,
            "description": description,
            "displayStoreMenu": object.displayStoreMenu,
            "name": name,
            "paymentInstructions": paymentInstructions,
            "phone": phone,
            "website": website,
            "welcomeMessage": welcomeMessage
          ]
        case .loadAllStore:
            return nil
        case .savePage(let param):
          return param
        case .checkApi:
            return  [
              "pageIndex": "1",
              "pageSize": "10",
              "reverse":"true",
              "sortBy": "createdDate"
          ]
        case .createItem(let nameItem, let sellAmount):
          return [
            "code": "",
            "id": 0,
            "description": "",
            "name": nameItem,
            "quantity": 1,
            "sellPrice": sellAmount,
            "trackInventory": "false"
          ]
        case .createLinkforUploadImage(let IDItem, let itemCount):
          return [
            "blobName": "items/"+IDItem+"/full/\(itemCount).jpg"
          ]
        case .createLinkforUploadImageLogo(let storeId):
          return [
            "blobName": "stores/\(storeId)/logo.png"
          ]
      }
    }
    
    var parameterEncoding: ParameterEncoding {
      if method == .get {
        return URLEncoding.default
      }
        switch self {
        case .login:
            return URLEncoding.default // Send parameters in URL
        case .register, .updateStoreInfo:
            return JSONEncoding.prettyPrinted // Send parameters as JSON in request body
        case .externalLogin, .savePage:
            return URLEncoding.httpBody // Send parameters in Raw data
        case .loadData,
             .createItem,
             .checkApi,
             .loadOrder,
             .loadAllStore,
             .loadItemInStore,
             .createLinkforUploadImage,
             .createLinkforUploadImageLogo:
            return JSONEncoding.default
        }
    }
  
    var headers: [String : String]? {
      
      switch self {
      case .login, .externalLogin:
        return ["Content-Type": "application/x-www-form-urlencoded"]
        
      case .register:
        return ["Content-Type": "application/json"]
        
      case .createLinkforUploadImage, .createLinkforUploadImageLogo:
        let user = UserDefaults.standard.string(forKey: "UserToken") ?? ""
        return ["Authorization": user]
        
      case .loadData, .loadOrder, .loadAllStore, .checkApi, .createItem, .updateStoreInfo:
        let user = UserDefaults.standard.string(forKey: "UserToken") ?? ""
        return ["Content-Type": "application/json", "Authorization": user]
        
      default:
        return [:]
      }
    }
  
    public var validate: Bool {
        switch self {
//        case .login:
//            return true
        default:
            return false
        }
    }
    
    var sampleData: Data {
        switch self {
        case .login(let username, let password):
            return "{\"username\": \(username), \"password\": \(password)}".data(using: String.Encoding.utf8)!
        case .createItem(let nameItem, let sellAmount):
            return "{\"nameItem\": \(nameItem), \"sellAmount\": \(sellAmount)}".data(using: String.Encoding.utf8)!
        case .register(let email, let password):
          return "{\"username\": \(email), \"password\": \(password)}".data(using: String.Encoding.utf8)!
        case .externalLogin(let provider, let email, let accessToken):
            return "{\"provider\": \(provider), \"email\": \(email), \"accessToken\": \(accessToken)}".data(using: String.Encoding.utf8)!
        case .loadData(let type, let page):
            return "{\"type\": \(type), \"page\": \(page)}".data(using: String.Encoding.utf8)!
        case .loadOrder(let page):
          return "{\"page\": \(page)}".data(using: String.Encoding.utf8)!
        case .checkApi:
            return "Check Api.".data(using: String.Encoding.utf8)!
        case .savePage(let parameter):
          return "{\"parameter\": \(parameter)}".data(using: String.Encoding.utf8)!
        case .createLinkforUploadImageLogo(let storeID):
          return "{\"storeID\": \(storeID)}".data(using: String.Encoding.utf8)!
        case .loadAllStore:
          return "LoadAll Api Api.".data(using: String.Encoding.utf8)!
        case .loadItemInStore(let pageName):
          return "{\"Load Item\": \(pageName)}".data(using: String.Encoding.utf8)!
        case .createLinkforUploadImage(let IDItem, let ItemCount):
          return "{\"IDItem\": \(IDItem), \"ItemCount\": \(ItemCount)}".data(using: String.Encoding.utf8)!
        case .updateStoreInfo(_):
          return "UpdateStoreInfo.".data(using: String.Encoding.utf8)!
      }
    }
    
    var task: Task {
      return .requestParameters(parameters: self.parameters ?? [:], encoding: self.parameterEncoding)
    }
  
}

// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
}

public func url(_ route: TargetType) -> String {
    return route.baseURL.appendingPathComponent(route.path).absoluteString
}

//MARK: - Response Handlers

extension Moya.Response {
    func mapNSArray() throws -> NSArray {
        let any = try self.mapJSON()
        guard let array = any as? NSArray else {
            throw MoyaError.jsonMapping(self)
        }
        return array
    }
}

private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}
