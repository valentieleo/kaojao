//
//  ConnectAPI.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 9/26/2559 BE.
//  Copyright © 2559 Tana Chaijamorn. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireObjectMapper
import ObjectMapper

class ConnectAPI<T : Mappable>: NSObject {
    
    static func login(_ username:String,password:String,
                  completionHandler: @escaping (T?, Error?) -> ()) {
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let parameters = [
            "grant_type": "password",
            "username":username,
            "password":password
        ]
        
        let url :String = String.init(format: "https://api.kaojao.com/token")
        Alamofire.request(url, method: HTTPMethod.post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers)
            .responseObject(completionHandler: { (response: DataResponse<T>) in
            
            switch response.result {
                case .success:
                    completionHandler(response.result.value, nil)
                    break;
                case .failure(let error):
                    completionHandler(nil, error)
                    break;
            }
            
        })
    }
    
    func register(_ username:String,password:String,
                      completionHandler: @escaping (NSDictionary?, NSError?) -> ()) {
        let headers = [
            "Content-Type": "application/json"
        ]
        let parameters = [
            "email":username,
            "password":password,
            "confirmPassword":password
        ]
        
        let url :String = String.init(format: "https://api.kaojao.com/api/user/register")
        Alamofire.request(url, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: headers)
            .responseJSON { response in
                
                debugPrint(response)
                if response.response?.statusCode == 200 {
                    switch response.result {
                        
                    case .success(let JSON):
                        completionHandler(JSON as? NSDictionary,nil)
                    case .failure(let error):
                        completionHandler(nil, error as NSError?)
                    }
                } else {
                    completionHandler(nil, nil)
                }
                
        }
        
    }
    

    static func externalLogin(_ provider: String, accessToken:String,email:String,
                      completionHandler: @escaping (T?, Error?) -> ()) {
        let headers = [
            "Accept": "application/json"
        ]
        let parameter = [
            "provider": provider,
            "email": email,
            "externalAccessToken": accessToken
        ]
        
        let url :String = String.init(format: "https://api.kaojao.com/api/User/ExternalLogin")
        Alamofire.request(url, method: HTTPMethod.post, parameters: parameter, encoding: URLEncoding.httpBody, headers: headers)
            .responseObject(completionHandler: { (response: DataResponse<T>) in
            
                switch response.result {
                case .success:
                    completionHandler(response.result.value, nil)
                    break;
                case .failure(let error):
                    completionHandler(nil, error)
                    break;
                }
            
            })
    }
    
    static func loadData(_ method:String, user:String, page:Int,
                         completionHandler: @escaping (T?, Error?) -> ()) {
        if method == "Income" {
            let headers = [
                "Authorization": user,
                "Accept": "application/json"
            ]
            let url :String = String.init(format: "https://api.kaojao.com/api/incomes/list?pageIndex=%d&pageSize=10&sortBy=incomeDate", page)
            Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                .responseObject(completionHandler: { (response: DataResponse<T>) in
                    
                    switch response.result {
                    case .success:
                        completionHandler(response.result.value, nil)
                        break;
                    case .failure(let error):
                        completionHandler(nil, error)
                        break;
                    }
                    
                })
        }else if method == "Estimate" {
            let headers = [
                "Authorization": user,
                "Accept": "application/json"
            ]
            let url :String = String.init(format: "https://api.kaojao.com/api/estimates/list?pageIndex=%d&pageSize=10&sortBy=estimateDate", page)
            Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                .responseObject(completionHandler: { (response: DataResponse<T>) in
                    
                    switch response.result {
                    case .success:
                        completionHandler(response.result.value, nil)
                        break;
                    case .failure(let error):
                        completionHandler(nil, error)
                        break;
                    }
                    
                })
        }else if method == "Expenses" {
            let headers = [
                "Authorization": user,
                "Accept": "application/json"
            ]
            let url :String = String.init(format: "https://api.kaojao.com/api/expenses/list?pageIndex=%d&pageSize=10&sortBy=expenseDate", page)
            Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                .responseObject(completionHandler: { (response: DataResponse<T>) in
                    
                    switch response.result {
                    case .success:
                        completionHandler(response.result.value, nil)
                        break;
                    case .failure(let error):
                        completionHandler(nil, error)
                        break;
                    }
                    
                })
        }else if method == "Clients" {
            let headers = [
                "Authorization": user,
                "Accept": "application/json"
            ]
            let url :String = String.init(format: "https://api.kaojao.com/api/clients/list?pageIndex=%d&pageSize=10&reverse=true&sortBy=createdDate", page)
            Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                .responseObject(completionHandler: { (response: DataResponse<T>) in
                    
                    switch response.result {
                    case .success:
                        completionHandler(response.result.value, nil)
                        break;
                    case .failure(let error):
                        completionHandler(nil, error)
                        break;
                    }
                    
                })
        }else if method == "Items" {
            let headers = [
                "Authorization": user,
                "Accept": "application/json"
            ]
            let url :String = String.init(format: "https://api.kaojao.com/api/items/list?pageIndex=%d&pageSize=10&reverse=true&sortBy=createdDate", page)
            Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                .responseObject(completionHandler: { (response: DataResponse<T>) in
                    
                    switch response.result {
                    case .success:
                        completionHandler(response.result.value, nil)
                        break;
                    case .failure(let error):
                        completionHandler(nil, error)
                        break;
                    }
                    
                })
        }
    }
    
    static func loadOrder(_ user:String, page:Int,
                         completionHandler: @escaping (T?, Error?) -> ()) {
            let headers = [
                "Authorization": user,
                "Accept": "application/json"
            ]
            let url :String = String.init(format: "https://api.kaojao.com/api/orders/list?pageIndex=%d&pageSize=10&reverse=true&sortBy=createdDate", page)
            Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                .responseObject(completionHandler: { (response: DataResponse<T>) in
                    
                    switch response.result {
                    case .success:
                        completionHandler(response.result.value, nil)
                        break;
                    case .failure(let error):
                        completionHandler(nil, error)
                        break;
                    }
                    
                })
    }
    
    func checkAPI(_ token:String, completionHandler: @escaping (NSDictionary?, NSError?) -> ())  {
        let headers = [
            "Authorization": token,
            "Accept": "application/json"
        ]
        let url :String = String.init(format: "https://api.kaojao.com/api/incomes/list?pageIndex=1&pageSize=10&sortBy=incomeDate")
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                switch response.result {
                    
                case .success(let JSON):
                    completionHandler(JSON as? NSDictionary,nil)
                case .failure(let error):
                    completionHandler(nil, error as NSError?)
                }
        }
    }
    
    static func createItem(_ user:String, nameString:String, sellInt:NSInteger,
                    completionHandler: @escaping (T?, Error?) -> ())  {
        let headers = [
            "Authorization": user,
            "Accept": "application/json"
        ]
        let parameter = [
            "code": "",
            "id": 0,
            "description": "",
            "name": nameString,
            "quantity": 1,
            "sellPrice": sellInt,
            "trackInventory": "false"
        ] as [String : Any]
        let url :String = String.init(format: "https://api.kaojao.com/api/items/create")
        
        Alamofire.request(url, method: HTTPMethod.post, parameters: parameter, encoding: URLEncoding.httpBody, headers: headers)
            
            .responseObject(completionHandler: { (response: DataResponse<T>) in
                
                switch response.result {
                case .success:
                    completionHandler(response.result.value, nil)
                    break;
                case .failure(let error):
                    completionHandler(nil, error)
                    break;
                }
                
            })
    }
    
    static func createLinkforUploadImage(_ IDItem: String,ItemCount: Int,
                                  completionHandler: @escaping (T?, Error?) -> ()) {
        let token = UserDefaults.standard.value(forKey: "UserToken") as? String
        
        let itemCountString = String(ItemCount)
        
        let headers = [
            "Authorization": token!
        ]
        
        let url :String = String.init(format: "https://api.kaojao.com/api/storage/getwritesas?blobName=items/"+IDItem+"/full/"+itemCountString+".jpg")
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseObject(completionHandler: { (response: DataResponse<T>) in
                
                switch response.result {
                case .success:
                    completionHandler(response.result.value, nil)
                    break;
                case .failure(let error):
                    completionHandler(nil, error)
                    break;
                }
                
            })
    }
    
    static func createLinkforUploadImageLogo(_ storeID: String, token: String,
                                         completionHandler: @escaping (T?, Error?) -> ()) {
        
        let headers = [
            "Authorization": token
        ]
        
        let url :String = String(format: "https://api.kaojao.com/api/storage/getwritesas?blobName=stores/%@/logo.png",storeID)
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseObject(completionHandler: { (response: DataResponse<T>) in
                
                switch response.result {
                case .success:
                    completionHandler(response.result.value, nil)
                    break;
                case .failure(let error):
                    completionHandler(nil, error)
                    break;
                }
                
            })
    }
    
    func uploadImageToServer(_ urlItem: String,imageData: Data,
                                    completionHandler: @escaping (T?, Error?) -> ()){
        Alamofire.upload(imageData, to: urlItem, method: HTTPMethod.put, headers: ["x-ms-blob-type": "BlockBlob"])
            .response { response in
                debugPrint(response)
        }
    }
    
    func createCompany(_ usertoken: String, companyNameText: String,
                 completionHandler: @escaping (NSDictionary?, NSError?) -> ()) {
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization":usertoken
        ]
        
        let parameter = [
            "companyName": companyNameText,
            "address1": "",
            "address2": "",
            "phone": ""
        ]
        
        let url :String = String.init(format: "https://api.kaojao.com/api/company/update/partial")
        Alamofire.request(url, method: .put, parameters: parameter, encoding: JSONEncoding.prettyPrinted, headers: headers)
            .responseJSON { response in
                
                debugPrint(response)
                
                if response.response?.statusCode == 200 {
                    switch response.result {
                        
                    case .success(let JSON):
                        completionHandler(JSON as? NSDictionary,nil)
                    case .failure(let error):
                        completionHandler(nil, error as NSError?)
                    }
                } else {
                    completionHandler(nil, nil)
                }
                
        }
    }
    
    func createStore(_ usertoken: String,
                       completionHandler: @escaping (NSDictionary?, Error?) -> ()) {
        let headers = [
            "Content-Type": "application/json",
            "Authorization":usertoken
        ]
        
        let url :String = String.init(format: "https://api.kaojao.com/api/stores/first")
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                
                debugPrint(response)
                if response.response?.statusCode == 200 {
                    switch response.result {
                        
                    case .success(let JSON):
                        completionHandler(JSON as? NSDictionary,nil)
                    case .failure(let error):
                        completionHandler(nil, error as NSError?)
                    }
                } else {
                    completionHandler(nil, nil)
                }
            }
            
//            .responseObject(completionHandler: { (response: DataResponse<T>) in
//                
//                switch response.result {
//                case .success:
//                    completionHandler(response.result.value, nil)
//                    break;
//                case .failure(let error):
//                    completionHandler(nil, error)
//                    break;
//                }
//                
//            })
    }
    
    func loadAllStore(_ usertoken: String,
                     completionHandler: @escaping (Array<Any>?, Error?) -> ()) {
        let headers = [
            "Content-Type": "application/json",
            "Authorization":usertoken
        ]
        
        let url :String = String.init(format: "https://api.kaojao.com/api/stores/list")
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                
//                debugPrint(response)
                if response.response?.statusCode == 200 {
                    switch response.result {
                    case .success(let JSON):
                        completionHandler(JSON as? Array,nil)
                    case .failure(let error):
                        completionHandler(nil, error as NSError?)
                    }
                } else {
                    completionHandler(nil, nil)
                }
        }
    }
  
  func loadAllSelectPage(_ usertoken: String, storeID: String,
                    completionHandler: @escaping (Array<Any>?, Error?) -> ()) {
    let headers = [
      "Content-Type": "application/json",
      "Authorization":usertoken
    ]
    
    let url :String = String.init(format: "https://api.kaojao.com/api/stores/getstorefblist?storeId=%@",storeID)
    Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
      .responseJSON { response in
        if response.response?.statusCode == 200 {
          switch response.result {
          case .success(let JSON):
            let jsonArray = JSON as! [[String: Any]]
            let pageResoponses = [PageResponse].init(JSONArray: jsonArray)
            completionHandler(pageResoponses,nil)
          case .failure(let error):
            completionHandler(nil, error as NSError?)
          }
        } else {
          completionHandler(nil, nil)
        }
    }
  }
  
    static func loadAllItemStore(_ usertoken: String, storeName: String, indexpage: Int,
                      completionHandler: @escaping (T?, Error?) -> ()) {
        let headers = [
            "Content-Type": "application/json",
            "Authorization":usertoken
        ]
        
        let url :String = String.init(format: "https://api.kaojao.com/api/items/items?pageName=%@&pageIndex=1&pageSize=20&reverse=true&sortBy=Name",storeName,indexpage)
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseObject(completionHandler: { (response: DataResponse<T>) in
                
                switch response.result {
                case .success:
                    completionHandler(response.result.value, nil)
                    break;
                case .failure(let error):
                    completionHandler(nil, error)
                    break;
                }
                
            })
    }
  
  static func getstorelist(_ facebooktoken: String,
                               completionHandler: @escaping (T?, Error?) -> ()) {
    
    let url :String = String.init(format: "https://graph.facebook.com/me/accounts?access_token=%@",facebooktoken)
    
    Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil)
      .responseObject(completionHandler: { (response: DataResponse<T>) in
        
        switch response.result {
        case .success:
          completionHandler(response.result.value, nil)
          break;
        case .failure(let error):
          completionHandler(nil, error)
          break;
        }
        
      })
  }
  
    func updateStore(_ usertoken: String, idStore: String, addressLinkStore: String, nameStore: String, dataStore: NSDictionary, descriptionStore: String, phoneStore: String, contactInfo1: String, contactInfo2: String, contactInfo3: String,
                       completionHandler: @escaping (NSDictionary?, NSError?) -> ()) {
        
        let headers = [
            "Content-Type": "application/json",
            "Authorization":usertoken
        ]
        
        let parameter = [
            "id":idStore,
            "address":addressLinkStore,
            "name":nameStore,
            "description":descriptionStore,
            
            "phone":phoneStore,
            "contactInfo1":contactInfo1,
            "contactInfo2":contactInfo2,
            "contactInfo3":contactInfo3,
            
            "isActive":true
        ] as [String : Any]
      
      
        let url :String = String.init(format: "https://api.kaojao.com/api/stores/update")
        
        Alamofire.request(url, method: .put, parameters: parameter, encoding: JSONEncoding.prettyPrinted, headers: headers)
            .responseJSON { response in
                
                debugPrint(response)
                
                if response.response?.statusCode == 200 {
                    switch response.result {
                        
                    case .success(let JSON):
                        completionHandler(JSON as? NSDictionary,nil)
                    case .failure(let error):
                        completionHandler(nil, error as NSError?)
                    }
                } else {
                    completionHandler(nil, nil)
                }
                
        }
    }
  
  func newUpdateStore(_ object: ListDataResponse,
                   completionHandler: @escaping (NSDictionary?, NSError?) -> ()) {
    let usertoken = UserDefaults.standard.string(forKey: "UserToken") ?? ""
    let headers = [
      "Content-Type": "application/json",
      "Authorization":usertoken
    ]
    
    let parameter = [
      "id" : object.id?.toString,
      "address" : object.address ?? "",
      "name" : object.nameStore ?? "",
      "description" : object.description ?? "",
      "website" : object.website ?? "",
      
      "phone" : object.phone ?? "",
      "contactInfo1" : object.contactInfo1 ?? "",
      "contactInfo2" : object.contactInfo2 ?? "",
      "contactInfo3" : object.contactInfo3 ?? "",
      
      "welcomeMessage" : object.welcomeMessage ?? "",
      "paymentInstructions" : object.paymentInstructions ?? "",
      "displayStoreMenu" : object.displayStoreMenuString ?? ""
      ] as [String : AnyObject]
    
    
    let url :String = String.init(format: "https://api.kaojao.com/api/stores/update")
    
    Alamofire.request(url, method: .put, parameters: parameter, encoding: JSONEncoding.prettyPrinted, headers: headers)
      .responseJSON { response in
        
        debugPrint(response)
        
        if response.response?.statusCode == 200 {
          switch response.result {
            
          case .success(let JSON):
            completionHandler(JSON as? NSDictionary,nil)
          case .failure(let error):
            completionHandler(nil, error as NSError?)
          }
        } else {
          completionHandler(nil, nil)
        }
        
    }
  }
    func storeDetail(_ token:String, storeid:String, completionHandler: @escaping (NSDictionary?, NSError?) -> ())  {
        let headers = [
            "Authorization": token,
            "Accept": "application/json"
        ]
        let url :String = String.init(format: "https://api.kaojao.com/api/stores/detail?id=%@",storeid)
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                switch response.result {
                    
                case .success(let JSON):
                    completionHandler(JSON as? NSDictionary,nil)
                case .failure(let error):
                    completionHandler(nil, error as NSError?)
                }
        }
    }
  
  func savePage(_ token:String, parameters:Dictionary<String, Any>,
                completionHandler: @escaping (NSDictionary?, NSError?) -> ()) {
    let headers = [
      "Authorization": token,
      "Content-Type": "application/json"
    ]
    
    let url :String = String.init(format: "https://api.kaojao.com/api/stores/connectFBPages")
    Alamofire.request(url, method: HTTPMethod.post, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: headers)
      .responseJSON { response in
        
        debugPrint(response)
        if response.response?.statusCode == 200 {
          switch response.result {
            
          case .success(let JSON):
            completionHandler(JSON as? NSDictionary,nil)
          case .failure(let error):
            completionHandler(nil, error as NSError?)
          }
        } else {
          completionHandler(nil, nil)
        }
        
    }
    
  }
}
