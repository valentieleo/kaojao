//
//  KaojaoWorker.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 31/8/17.
//  Copyright © 2017 Tana Chaijamorn. All rights reserved.
//

import Foundation
import Moya
import Result

enum APIError: Swift.Error {
  case serverError
  case unAuthorization
  case failLogin
  case emailUsed
}

class KaojaoWorker {
  
  class func loginAPI(username: String, password: String, completion: @escaping (Result<LoginResponse, APIError>) -> Void) {
    
    kaojaoProvider.request(.login(username: username, password: password)) { result in
      
      switch result {
      case let .success(response):
        let responseCode = response.statusCode
        var login: LoginResponse? = nil
        switch responseCode {
          case 200:
              do {
                login = try response.mapObject(LoginResponse.self)
                
              } catch { }
          case 401:
            completion(.failure(.unAuthorization))
        default:
          print("failLogin")
        }
        
        guard let logindata = login else {
          completion(.failure(.failLogin))
          return
        }
        
        completion(.success(logindata))
        
      case .failure(let error):
        print(error.localizedDescription)
        
        completion(.failure(.serverError))
        
      }
    }
  }
  
  class func externalLoginAPI(provider: String, email: String, accessToken: String, completion: @escaping (Result<ExternalLoginResponse, APIError>) -> Void) {
    
    kaojaoProvider.request(kaojaoService.externalLogin(provider: provider, email: email, accessToken: accessToken)) { result in
      switch result {
      case .success(let response):
        let responseCode = response.statusCode
        var loginEx: ExternalLoginResponse? = nil
        switch responseCode {
        case 200:
          do {
            loginEx = try response.mapObject(ExternalLoginResponse.self)
          } catch {}
        case 401:
          completion(.failure(.unAuthorization))
        default:
          print("failLogin")
        }
        
        guard let loginData = loginEx else{
          completion(.failure(.failLogin))
          return
        }
        completion(.success(loginData))
        
      case .failure(let error):
        print(error.localizedDescription)
        completion(.failure(.serverError))
      }
    }
  }
  
  class func register(email: String, password: String, completion: @escaping (Result<String, APIError>) -> Void) {
    
    kaojaoProvider.request(.register(email: email, password: password)) { result in
      
      switch result {
      case let .success(response):
        let responseCode = response.statusCode
        
        switch responseCode {
        case 200:
          completion(.success("success"))
        case 400:
          completion(.failure(.emailUsed))
        default:
          print("failRegister")
        }
        
      case .failure(let error):
        print(error.localizedDescription)
        
        completion(.failure(.serverError))
        
      }
    }
  }
  
  class func loadItem(type: String, index: Int, completion: @escaping (Result<ListDataResponse, APIError>) -> Void) {

    var typeString = ""
    if (type == "") {
      typeString = "Items"
    }else{
      typeString = type
    }
    
    kaojaoProvider.request(.loadData(type: typeString, page: index)) { result in
      switch result {
      case .success(let response):
        let responseCode = response.statusCode
        var items: ListDataResponse? = nil
        
        switch responseCode {
        case 200:
          do {
            items = try response.mapObject(ListDataResponse.self)
            
          } catch { }
        case 401:
          completion(.failure(.unAuthorization))
        default:
          print("failLoadData")
        }
        
        guard let allData = items else {
          completion(.failure(.serverError))
          return
        }
        
        completion(.success(allData))
      case .failure(let error):
        print(error.localizedDescription)
        
        completion(.failure(.serverError))
      }
    }
  }
  
  class func loadStore(completion: @escaping (Result<[ListDataResponse], APIError>) -> Void) {

    kaojaoProvider.request(.loadAllStore) { result in

      switch result {
      case let .success(response):
        let responseCode = response.statusCode
        var store = [ListDataResponse]()
        
        switch responseCode {
        case 200:
          do {
            store = try response.mapArray(ListDataResponse.self)

          } catch { }
        case 401:
          completion(.failure(.unAuthorization))
        default:
          print("failLoadStore")
        }

        completion(.success(store))

      case .failure(let error):
        print(error.localizedDescription)

        completion(.failure(.serverError))

      }
    }
  }

  
  class func createItem(name: String, price: Float, completion: @escaping (Result<CreateItemResponse, APIError>) -> Void) {
    
    kaojaoProvider.request(.createItem(nameItem: name, sellAmount: price)) { result in
      
      switch result {
      case let .success(response):
        let responseCode = response.statusCode
        var item : CreateItemResponse? = nil
        
        switch responseCode {
        case 200:
          do {
            item = try response.mapObject(CreateItemResponse.self)
            
          } catch { }
        case 401:
          completion(.failure(.unAuthorization))
        default:
          print("failtoCreateItem")
        }
        
        guard let itemdata = item else {
          completion(.failure(.serverError))
          return
        }

        completion(.success(itemdata))
        
      case .failure(let error):
        print(error.localizedDescription)
        
        completion(.failure(.serverError))
        
      }
    }
  }
}
