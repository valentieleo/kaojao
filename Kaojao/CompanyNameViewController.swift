//
//  CompanyNameViewController.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 10/25/16.
//  Copyright © 2016 Tana Chaijamorn. All rights reserved.
//

import UIKit
import MBProgressHUD

class CompanyNameViewController: UIViewController {

    var userCode:String?
    var StoreCode:String?
    
    let reachability = Reachability()
    
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var companyNameTextfield: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setNavigationbarToTranspalent()
        self.title = NSLocalizedString("signUp", comment: "signUp")
        
        self.companyNameLabel.text = NSLocalizedString("companyName", comment: "companyName")
        self.companyNameTextfield.placeholder = NSLocalizedString("companyName", comment: "companyName")
        
        self.nextButton.setTitle(NSLocalizedString("next", comment: "next"), for: UIControlState.normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func nextFunc(_ sender: AnyObject) {
        if Reachability.isConnectedToNetwork() == true {
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = MBProgressHUDMode.indeterminate
            loadingNotification.label.text = "Loading"
            
            if companyNameTextfield.text?.characters.count == 0 {
            
                MBProgressHUD.hide(for: self.view, animated: true)
            } else {
              
                let api = ConnectAPI<ErrorReponse>()
                api.createCompany(userCode!, companyNameText: self.companyNameTextfield.text!) { (respon, error) in
                    if respon == nil && error == nil {
                    
                        MBProgressHUD.hide(for: self.view, animated: true)
                    } else if error != nil {
                        MBProgressHUD.hide(for: self.view, animated: true)

                        self.performSegue(withIdentifier: "toaddAddress", sender: nil)
                    }
                }
            }
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            VTAlertView()
                .title("No Internet Connection")
                .message("Make sure your device is connected to the internet.")
                .type(.alert)
                .button("OK", action: {
                    
                })
                .show(self)
        }
    }
    
    func setNavigationbarToTranspalent() {
        self.navigationController?.isNavigationBarHidden = false
        
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        
        if (bar == nil) {
            
        }else{
            bar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            bar.shadowImage = UIImage()
//            bar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
          bar.backgroundColor = UIColor(hex: "#37414d")
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
                if segue.identifier == "toaddAddress" {
                    if let AddressVC = segue.destination as? AddressRegisterViewController {
                        AddressVC.userCode = self.userCode
                        AddressVC.StoreCode = self.StoreCode
                    }
                }
    }

}
