//
//  RegisterViewController.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 10/23/16.
//  Copyright © 2016 Tana Chaijamorn. All rights reserved.
//

import UIKit
import MBProgressHUD
import EZSwiftExtensions

class RegisterViewController: UIViewController {

    @IBOutlet weak var registerLabel: UILabel!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var comfirmPasswordTextfield: UITextField!
    
    var user:String?
    var storeID:String?
    
    let reachability = Reachability()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationbarToTranspalent()
        self.title = NSLocalizedString("signUp", comment: "signUp")
        self.registerLabel.text = NSLocalizedString("signUp", comment: "signUp")
        self.registerButton.setTitle(NSLocalizedString("signUp", comment: "signUp"), for: UIControlState.normal)
      
        self.comfirmPasswordTextfield.placeholder = NSLocalizedString("confirmPassword", comment: "confirmPassword")
        self.emailTextfield.placeholder = NSLocalizedString("email", comment: "email")
        self.passwordTextfield.placeholder = NSLocalizedString("password", comment: "password")
        
    }
    
    @IBAction func registerfunc(_ sender: AnyObject) {
        if Reachability.isConnectedToNetwork() == true {
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = MBProgressHUDMode.indeterminate
            loadingNotification.label.text = "Loading"
            
            let emailText = self.emailTextfield.text
            let passwordText = self.passwordTextfield.text
            let confirmPasswordText = self.comfirmPasswordTextfield.text
            
            if self .isValidEmail(testStr: emailText!) == true {
                if (passwordText?.characters.count)! > 0 && (confirmPasswordText?.characters.count)! > 0 && passwordText! == confirmPasswordText! {
                  
                    let api = ConnectAPI<ErrorReponse>()
                    api.register(emailText!, password: passwordText!) { (responseData, error) in
                        
                        if error == nil && responseData == nil {
                            //alert
                            MBProgressHUD.hide(for: self.view, animated: true)
                            VTAlertView()
                                .title("Kaojao")
                                .message("Email is already used.")
                                .type(.alert)
                                .button("OK", action: {
                                    
                                })
                                .show(self)
                        }else if (error != nil) {
                            ConnectAPI<LoginResponse>.login(emailText!, password: passwordText!, completionHandler: { (responseData, error) in
                                let token_type = responseData?.token_type!
                                let access_token = responseData?.access_token!
                                
                                self.user = token_type!+" "+access_token!
                                
                                
                                ConnectAPI<ErrorReponse>()
                                    .createStore(self.user!, completionHandler: { (res, err) in
                                    let idStore = res?.object(forKey: "id") as? Int
                                    let userId = res?.object(forKey: "userId")!
                                    self.storeID = idStore?.toString
                                    
                                    UserDefaults.standard.set(userId, forKey: "UserCodeUpload")
//                                    self.storeID = res?.storeID
                                    MBProgressHUD.hide(for: self.view, animated: true)
                                    VTAlertView()
                                        .title("Kaojao")
                                        .message("Register Done, Please update your company info")
                                        .type(.alert)
                                        .button("OK", action: {
                                            self.performSegue(withIdentifier: "toAddressCom", sender: nil)
                                        })
                                        .show(self)
                                })
                                
                            })
                        }else {
                            //alert
                            debugPrint("error?")
                        }
                    }
                }else{
                    MBProgressHUD.hide(for: self.view, animated: true)
                    VTAlertView()
                        .title("Kaojao")
                        .message("No Password or Password not matching")
                        .type(.alert)
                        .button("OK", action: {
                            
                        })
                        .show(self)
                }
            }else{
                MBProgressHUD.hide(for: self.view, animated: true)
                debugPrint("no email")
                VTAlertView()
                    .title("Kaojao")
                    .message("No email")
                    .type(.alert)
                    .button("OK", action: {
                        
                    })
                    .show(self)
            }
        } else {
            MBProgressHUD.hide(for: self.view, animated: true)
            VTAlertView()
                .title("No Internet Connection")
                .message("Make sure your device is connected to the internet.")
                .type(.alert)
                .button("OK", action: {
                    
                })
                .show(self)
        }
        
        
        
    }

    func setNavigationbarToTranspalent() {
        self.navigationController?.isNavigationBarHidden = false
        
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        
        if (bar == nil) {
            
        }else{
            bar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            bar.shadowImage = UIImage()
//            bar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
          bar.backgroundColor = UIColor(hex: "#37414d")
        }
        
    }

    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toaddCompany" {
            if let CompanyVC = segue.destination as? CompanyNameViewController {
                CompanyVC.userCode = self.user
                CompanyVC.StoreCode = self.storeID
            }
        }else if (segue.identifier == "toAddressCom") {
          if let addressVC = segue.destination as? AddressRegisterViewController {
            addressVC.userCode = self.user
            addressVC.StoreCode = self.storeID
          }
      }
    }
}
