//
//  DetailIncomeViewController.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 10/1/16.
//  Copyright © 2016 Tana Chaijamorn. All rights reserved.
//

import UIKit
import CCBottomRefreshControl
import MBProgressHUD

class DetailIncomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var dataList = [AnyObject]()
    var pages : Int = 1
    let api = ConnectAPI<LoginResponse>()
    let user : String = (UserDefaults.standard.value(forKey: "UserToken") as? String)!
    
    @IBOutlet var menuButton:UIBarButtonItem!
    @IBOutlet weak var tableincome: UITableView!
    
    var refreshControl: UIRefreshControl!
    
    var typelist:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.setRightBarButton(menuButton, animated: true)
        
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"
        
        self.tableincome.estimatedRowHeight = 68
        self.tableincome.rowHeight = UITableViewAutomaticDimension
        
        if typelist! == "Income" {
            self.title = NSLocalizedString("incomeTypeIncome", comment: "incomeTypeIncome")
            self.menuButton.image = UIImage(named: "")
            self.menuButton.isEnabled = false
        }else if typelist! == "Estimate" {
            self.title = NSLocalizedString("estimateTypeEstimate", comment: "estimateTypeEstimate")
            self.menuButton.image = UIImage(named: "")
            self.menuButton.isEnabled = false
        }else if typelist! == "Expenses" {
            self.title = NSLocalizedString("accCategoryExpenses", comment: "accCategoryExpenses")
            self.menuButton.image = UIImage(named: "")
            self.menuButton.isEnabled = false
        }else if typelist! == "Clients" {
            self.title = NSLocalizedString("clients", comment: "clients")
            self.menuButton.image = UIImage(named: "")
            self.menuButton.isEnabled = false
        }else if typelist! == "Items" {
            self.title = NSLocalizedString("items", comment: "items")
        }else{
            self.title = NSLocalizedString("incomeTypeIncome", comment: "incomeTypeIncome")
            self.menuButton.image = UIImage(named: "")
            self.menuButton.isEnabled = false
        }
        
        let type :String = self.typelist!
      
        ConnectAPI<ListDataResponse>.loadData(type, user: self.user, page: self.pages) { responseObject, error in
            self.dataList = (responseObject?.collection as AnyObject) as! [AnyObject]
            MBProgressHUD.hide(for: self.view, animated: true)
            self.tableincome.reloadData()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tableincome.reloadData()
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(DetailIncomeViewController.refresh(_:)), for: UIControlEvents.valueChanged)
        
        tableincome.bottomRefreshControl = refreshControl
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        refreshControl.removeFromSuperview()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @objc func refresh(_ sender:AnyObject) {
        // Code to refresh table view
        pages += 1
        
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"
        let type :String = self.typelist!
      
        ConnectAPI<ListDataResponse>.loadData(type, user: self.user, page: self.pages) { responseObject, error in
            self.dataList = (responseObject?.collection as AnyObject) as! [AnyObject]
            MBProgressHUD.hide(for: self.view, animated: true)
            self.tableincome.reloadData()
        }
        
        refreshControl.endRefreshing()
    }

    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if typelist! == "Income" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "IncomeCell", for: indexPath as IndexPath) as! IncomeTableViewCell
            if (indexPath.row % 2) != 0 {
                cell.backgroundColor = UIColor.init(hex: "F7F7F7")
            }else{
                cell.backgroundColor = UIColor.white
            }
            
            cell.ActivityName.text = dataList[indexPath.row]["clientContactName"] as? String
            cell.statusvalue.text = dataList[indexPath.row]["incomeStatus"] as? String
            cell.dateIncome.text = dataList[indexPath.row]["incomeDate"] as? String
            cell.Costvalue.text = dataList[indexPath.row]["totalAmount"] as? String
            cell.selectionStyle = .none
            
            return cell
        }else if typelist! == "Estimate" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "IncomeCell", for: indexPath as IndexPath) as! IncomeTableViewCell
            if (indexPath.row % 2) != 0 {
                cell.backgroundColor = UIColor.init(hex: "F7F7F7")
            }else{
                cell.backgroundColor = UIColor.white
            }
            
            cell.ActivityName.text = dataList[indexPath.row]["clientContactName"] as? String
            cell.statusvalue.text = dataList[indexPath.row]["estimateStatus"] as? String
            cell.dateIncome.text = dataList[indexPath.row]["estimateDate"] as? String
            cell.Costvalue.text = dataList[indexPath.row]["totalAmount"] as? String
            
            cell.selectionStyle = .none
            
            return cell
        }else if typelist! == "Expenses" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "IncomeCell", for: indexPath as IndexPath) as! IncomeTableViewCell
            if (indexPath.row % 2) != 0 {
                cell.backgroundColor = UIColor.init(hex: "F7F7F7")
            }else{
                cell.backgroundColor = UIColor.white
            }
            cell.selectionStyle = .none
            cell.statusvalue?.text = ""
            
            cell.ActivityName.text = dataList[indexPath.row]["vendorName"] as? String
            cell.dateIncome.text = dataList[indexPath.row]["expenseDate"] as? String
            cell.Costvalue.text = dataList[indexPath.row]["amount"] as? String
            
            return cell
        }else if typelist! == "Items" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ClientAndItemsCell", for: indexPath as IndexPath) as! ClientAndItemsCell
            if (indexPath.row % 2) != 0 {
                cell.backgroundColor = UIColor.init(hex: "F7F7F7")
            }else{
                cell.backgroundColor = UIColor.white
            }
            
            cell.selectionStyle = .none
            cell.itemLabel.text = dataList[indexPath.row]["name"] as? String
            
            if dataList[indexPath.row]["sellPrice"] as? NSNull ==  NSNull() {
                cell.priceLabel.text = ""
            }else{
                let price = dataList[indexPath.row]["sellPrice"] as! NSNumber
                cell.priceLabel.text = "\(price)"
            }
            
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "IncomeCell", for: indexPath as IndexPath) as! IncomeTableViewCell
            if (indexPath.row % 2) != 0 {
                cell.backgroundColor = UIColor.init(hex: "F7F7F7")
            }else{
                cell.backgroundColor = UIColor.white
            }
            
            cell.selectionStyle = .none
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if typelist! == "Income" {
            let DetailVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailInfoView") as? DetailInfoViewController
            DetailVC?.typelist = "Income"
            DetailVC?.dataList = dataList[indexPath.row] as! [String : AnyObject]
            self.navigationController?.pushViewController(DetailVC!, animated: true)
        }else if typelist! == "Estimate" {
            let DetailVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailInfoView") as? DetailInfoViewController
            DetailVC?.typelist = "Estimate"
            DetailVC?.dataList = dataList[indexPath.row] as! [String : AnyObject]
            self.navigationController?.pushViewController(DetailVC!, animated: true)
        }
    }
}
