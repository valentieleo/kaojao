//
//  LoginViewController.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 9/26/2559 BE.
//  Copyright © 2559 Tana Chaijamorn. All rights reserved.
//

import UIKit
import SwiftyJSON
import MBProgressHUD
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase
import Moya_ObjectMapper

class LoginViewController: UIViewController, FBSDKLoginButtonDelegate, UITextFieldDelegate, GIDSignInDelegate, GIDSignInUIDelegate {

    @IBOutlet weak var SignInLabel: UILabel!
    @IBOutlet weak var SignInBtn: UIButton!
    @IBOutlet weak var SignUpBtn: UIButton!
    @IBOutlet weak var ForgotPasswordBtn: UIButton!
    @IBOutlet weak var facebookBtn: FBSDKLoginButton!
    @IBOutlet weak var signInButton: GIDSignInButton!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet var menuButton:UIBarButtonItem!
    
    var push : Bool = false;
    let reachability = Reachability()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setNavigationbarToTranspalent()
        self.setUILanguage()
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        if (FBSDKAccessToken.current() != nil) {
            // User is already logged in, do work such as go to next view controller.
            self.facebookBtn.readPermissions = ["public_profile", "email"]
            self.facebookBtn.delegate = self
            //            self.returnUserData()
        } else {
            self.facebookBtn.readPermissions = ["public_profile", "email"]
            self.facebookBtn.delegate = self
        }
        

        if Reachability.isConnectedToNetwork() == true {
            
            var token = UserDefaults.standard.value(forKey: "UserToken") as? String
            if (token == nil) {
                token = ""
            }

            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = MBProgressHUDMode.indeterminate
            loadingNotification.label.text = "Loading"
          
          kaojaoProvider.request(.checkApi, completion: { result in
            switch result {
              case let .success(response):
                
                if response.statusCode == 401 {
                  MBProgressHUD.hide(for: self.view, animated: true)
                  UserDefaults.standard.removeObject(forKey: "UserToken")
                  UserDefaults.standard.removeObject(forKey: "FacebookToken")
                }else{
                  MBProgressHUD.hide(for: self.view, animated: true)
                  self.pushtoHomewithAnimation(animated: true)
                }
              case .failure(_):
                MBProgressHUD.hide(for: self.view, animated: true)
                UserDefaults.standard.removeObject(forKey: "UserToken")
                UserDefaults.standard.removeObject(forKey: "FacebookToken")
            }
          })
          
          
        } else {
            VTAlertView()
                .title("No Internet Connection")
                .message("Make sure your device is connected to the internet.")
                .type(.alert)
                .button("OK", action: {
                    
                })
                .show(self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - IBAction
    @IBAction func SignInFunc(sender: AnyObject) {
        self.emailField.resignFirstResponder()
        self.passwordField.resignFirstResponder()
        if isValidEmail(testStr: self.emailField.text!) {
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = MBProgressHUDMode.indeterminate
            loadingNotification.label.text = "Loading"
          
          KaojaoWorker.loginAPI(username: self.emailField.text!, password: self.passwordField.text!, completion: { result in
            
            switch result {
              case let .success(response):
              
                let token_type = response.token_type!
                let access_token = response.access_token!
                let user : String = token_type+" "+access_token
                UserDefaults.standard.set(user, forKey: "UserToken")
                MBProgressHUD.hide(for: self.view, animated: true)
                self.pushtoHomewithAnimation(animated: true)
              
              case .failure(_):
                
                self.setAlert(AlertStr: "Wrong Password")
                MBProgressHUD.hide(for: self.view, animated: true)
            }
          })
          
        }else{
            self.setAlert(AlertStr: "No Email")
        }
    }
    
    @IBAction func ForgetPasswordFunc(_ sender: AnyObject) {
    }
    
    @IBAction func googleLoginfunc(_ sender: AnyObject) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func facebookLoginfunc(_ sender: AnyObject) {
        let login = FBSDKLoginManager.init()
        let permission = ["public_profile", "email"]
        
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"
      
        login.logIn(withReadPermissions: permission, from: self) { (result, error) in
            
            if ((error) != nil) {
                MBProgressHUD.hide(for: self.view, animated: true)
                debugPrint("Process error")
            } else if (result?.isCancelled)! {
                MBProgressHUD.hide(for: self.view, animated: true)
                debugPrint("Cancelled")
            } else {
                debugPrint("Log in")
                
                let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name"])
                graphRequest.start(completionHandler: { (connection, result, error) -> Void in
                    
                    if ((error) != nil) {
                        // Process error
                        debugPrint("Error: \(String(describing: error))")
                    } else {
                        guard let result = result as? Dictionary<String, AnyObject> else {
                            self.setAlert(AlertStr: "fb api error")
                            return
                        }
                        
                        let userEmail = result["email"] as? String
                        UserDefaults.standard.set(FBSDKAccessToken.current().tokenString!, forKey: "FacebookToken")
                      
                      kaojaoProvider.request(.externalLogin(provider: "Facebook", email: userEmail!, accessToken: FBSDKAccessToken.current().tokenString), completion: { result in
                        switch result {
                        case let .success(response):
                          do {
                            let exlogin : ExternalLoginResponse? = try response.mapObject(ExternalLoginResponse.self)
                            
                            if let exlogin = exlogin {
                              let token_type = exlogin.token_type!
                              let access_token = exlogin.access_token!
                              let user : String = token_type+" "+access_token
                              UserDefaults.standard.set(user, forKey: "UserToken")
                              MBProgressHUD.hide(for: self.view, animated: true)
                              self.pushtoHomewithAnimation(animated: true)
                            }
                          } catch {
                            MBProgressHUD.hide(for: self.view, animated: true)
                          }
                        case .failure(_):
                          self.setAlert(AlertStr: "API ExternalLogin Error")
                          MBProgressHUD.hide(for: self.view, animated: true)
                        }
                      })
                    }
                    
                })
            }
        }
    }
    
    func setNavigationbarToTranspalent() {
        self.navigationController?.isNavigationBarHidden = false
        
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        
        if (bar == nil) {
            
        }else{
            bar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            bar.shadowImage = UIImage()
//            bar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
          bar.backgroundColor = UIColor(hex: "#37414d")
        }
    }
    
    func setUILanguage() {
        self.SignInLabel.text = NSLocalizedString("logIn", comment: "logIn")
        self.emailField.placeholder = NSLocalizedString("email", comment: "email")
        self.passwordField.placeholder = NSLocalizedString("password", comment: "password")
        let SignUpBtnTitle = NSMutableAttributedString(string: NSLocalizedString("signUp", comment: "signUp"))
        
        SignUpBtnTitle.addAttributes([NSAttributedStringKey.foregroundColor : UIColor.white], range: NSMakeRange(0, SignUpBtnTitle.length))
        SignUpBtnTitle.addAttributes([NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue], range: NSMakeRange(0, SignUpBtnTitle.length))
        
        let ForgotPasswordBtnTitle = NSMutableAttributedString(string: NSLocalizedString("forgetPassword", comment: "forgetPassword"))
        ForgotPasswordBtnTitle.addAttributes([NSAttributedStringKey.foregroundColor : UIColor.white], range: NSMakeRange(0, ForgotPasswordBtnTitle.length))
        ForgotPasswordBtnTitle.addAttributes([NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue], range: NSMakeRange(0, ForgotPasswordBtnTitle.length))
        
        self.SignUpBtn.setAttributedTitle(SignUpBtnTitle, for: UIControlState.normal)
        self.SignInBtn.setTitle(NSLocalizedString("logIn", comment: "logIn"), for: UIControlState.normal)
        self.ForgotPasswordBtn.setAttributedTitle(ForgotPasswordBtnTitle, for: UIControlState.normal)
    }
    // MARK: - Google Delegate
  
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"
        if let error = error {
            debugPrint(error.localizedDescription)
            return
        }
        
        let email : String = user.profile.email!
        let authentication = user.authentication
        let credential = GoogleAuthProvider.credential(withIDToken: (authentication?.idToken)!,
                                                          accessToken: (authentication?.accessToken)!)
        Auth.auth().signIn(with: credential) { (user, error) in
            
            let idToken : String = (authentication?.accessToken)!
          
          kaojaoProvider.request(.externalLogin(provider: "Google", email: email, accessToken: idToken), completion: { result in
            switch result {
            case let .success(response):
              MBProgressHUD.hide(for: self.view, animated: true)
              do {
                let exlogin : ExternalLoginResponse? = try response.mapObject(ExternalLoginResponse.self)
                
                if let exlogin = exlogin {
                  let token_type = exlogin.token_type!
                  let access_token = exlogin.access_token!
                  let user : String = token_type+" "+access_token
                  UserDefaults.standard.set(user, forKey: "UserToken")
                  MBProgressHUD.hide(for: self.view, animated: true)
                  self.pushtoHomewithAnimation(animated: true)
                }
              } catch {
                
              }
            case .failure(_):
              self.setAlert(AlertStr: "API ExternalLogin Error")
              MBProgressHUD.hide(for: self.view, animated: true)
            }
          })
          
        }
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    // MARK: - Facebook Delegate
    public func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        debugPrint("User Logged In")
        if error != nil {
            debugPrint("Process error")
            MBProgressHUD.hide(for: self.view, animated: true)
        } else if result.isCancelled {
            debugPrint("Handle cancellations")
            MBProgressHUD.hide(for: self.view, animated: true)
        } else {
            if result.grantedPermissions.contains("email") {
//                self.returnUserData()
            }
        }
    }
    
    
    public func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        debugPrint("User Logged Out")
    }
    
    // MARK: - Set TextFielddelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.emailField.resignFirstResponder()
        self.passwordField.resignFirstResponder()
        return true
    }
    
    // MARK: - Validate
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func isValidPassword(testStr2:String) -> Bool {
        
        let passwordRegEx = "[A-Z0-9a-z._%+-:/><#]{6,30}"
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegEx)
        
        return passwordTest.evaluate(with: testStr2)
    }
  
  func setAlert(AlertStr:String){
    VTAlertView()
      .title("Kaojao")
      .message(AlertStr)
      .type(.alert)
      .button("Dismiss", action: {
        
      })
      .show(self)
    
    let alertController = UIAlertController(title: "Kaojao", message:
      AlertStr, preferredStyle: UIAlertControllerStyle.alert)
    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
    
    self.present(alertController, animated: true, completion: nil)
  }
    // MARK: - Push View
    func pushtoHomewithAnimation(animated: Bool) {
        if self.revealViewController() != nil {
            print("have reveal")
            push = true
            let homeViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "HomeView") as? HomeViewController
            let navigation = UINavigationController.init(rootViewController: homeViewControllerObj!)
            self.revealViewController().pushFrontViewController(navigation, animated: animated)
        }
        
        
    }
}
