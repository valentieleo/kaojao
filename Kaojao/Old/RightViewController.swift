//
//  RightViewController.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 9/30/2559 BE.
//  Copyright © 2559 Tana Chaijamorn. All rights reserved.
//

import UIKit
import SWRevealViewController
import Firebase

class RightViewController: UIViewController {

    @IBOutlet weak var LogoutBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.LogoutBtn.setTitle(NSLocalizedString("logOut", comment: "logOut"), for: UIControlState.normal)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func LogOutFunc(sender: AnyObject) {
        try! Auth.auth().signOut()
        UserDefaults.standard.removeObject(forKey: "UserToken")
    }

}
