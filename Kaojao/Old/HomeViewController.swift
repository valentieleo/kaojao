//
//  HomeViewController.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 9/28/2559 BE.
//  Copyright © 2559 Tana Chaijamorn. All rights reserved.
//

import UIKit
import SWRevealViewController

class HomeViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
  
  var menulist = [String]()
  
  @IBOutlet weak var manuListCollection: UICollectionView!
  @IBOutlet weak var AddItemBtn: UIButton!
  @IBOutlet var menuButton:UIBarButtonItem!
  private let reuseIdentifier = "Cell"
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.navigationItem.setRightBarButton(menuButton, animated: true)
    self.navigationItem.setHidesBackButton(true, animated:true);
    
    if self.revealViewController() != nil {
      menuButton.target = self.revealViewController()
      menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
      self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
      self.AddItemBtn.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
    }
    
    self.AddItemBtn.setTitle(NSLocalizedString("newItem", comment: "newItem"), for: UIControlState.normal)
    
    self.menulist = [NSLocalizedString("chatbots", comment: "chatbots"),
                     NSLocalizedString("store", comment: "store"),
                     NSLocalizedString("items", comment: "items"),
                     "Messenger"]
    self.manuListCollection.reloadData()
    self.setNavigationbarToTranspalent()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - Collection Delegate
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.menulist.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! ManuCollectionViewCell
    
    cell.titleLabel.text = self.menulist[indexPath.row]
    
    if self.menulist[indexPath.row] == NSLocalizedString("chatbots", comment: "chatbots") {
      cell.imageView.image = UIImage(named: "shopping")
    }else if (self.menulist[indexPath.row] == NSLocalizedString("store", comment: "store")) {
      cell.imageView.image = UIImage(named: "store")
    }else if (self.menulist[indexPath.row] == "Messenger") {
      cell.imageView.image = UIImage(named: "messenger")
    }else{
      cell.imageView.image = UIImage(named: "items")
    }
    
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if indexPath.row == 0 {
      let OrderVC = self.storyboard?.instantiateViewController(withIdentifier: "ListItemView") as? ListItemViewController
      OrderVC?.type = "order"
      self.navigationController?.pushViewController(OrderVC!, animated: true)
    }else if (indexPath.row == 1){
      let OrderVC = self.storyboard?.instantiateViewController(withIdentifier: "ListItemView") as? ListItemViewController
      OrderVC?.type = "store"
      self.navigationController?.pushViewController(OrderVC!, animated: true)
    }else if (indexPath.row == 2){
      //            let DetailVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailIncomeView") as? DetailIncomeViewController
      //            DetailVC?.typelist = "Items"
      //            self.navigationController?.pushViewController(DetailVC!, animated: true)
      
      let ListItemVC = self.storyboard?.instantiateViewController(withIdentifier: "ListItemView") as? ListItemViewController
      self.navigationController?.pushViewController(ListItemVC!, animated: true)
    }else if (indexPath.row == 3){
      UIApplication.shared.openURL(URL(string: "http://m.me")!)
    }
    
  }
  
  func collectionView(_ collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize {
    let cellSize = CGSize(width: self.manuListCollection.frame.width/2-10, height: self.manuListCollection.frame.width/2)
    return cellSize
  }
  
  // MARK: - IBAction
  
  @IBAction func passonAddNew(_ sender: AnyObject) {
    let AddVC = self.storyboard?.instantiateViewController(withIdentifier: "AddDataView") as? AddDataViewController
    self.navigationController?.pushViewController(AddVC!, animated: true)
  }
  
  // MARK: - Set UI
  func setAlert(AlertStr:String){
    VTAlertView()
      .title("Kaojao")
      .message(AlertStr)
      .type(.alert)
      .button("Dismiss", action: {
        
      })
      .show(self)
  }
  
  func setNavigationbarToTranspalent() {
    self.navigationController?.isNavigationBarHidden = false
    
    let bar:UINavigationBar! =  self.navigationController?.navigationBar
    
    if (bar == nil) {
      
    }else{
      bar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
      bar.shadowImage = UIImage()
      bar.backgroundColor = UIColor(hex: "#37414d")
    }
    
  }
}
