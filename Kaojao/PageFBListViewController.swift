//
//  PageFBListViewController.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 14/7/2560 BE.
//  Copyright © 2560 Tana Chaijamorn. All rights reserved.
//

import UIKit
import MBProgressHUD
import WOWCheckbox
import ObjectMapper
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import EZSwiftExtensions

class PageFBListViewController: UIViewController,WOWCheckboxDelegate,UICollectionViewDelegate,UICollectionViewDataSource {

  var listPage = [PageResponse]()
  var storeID:String?
  private let reuseIdentifier = "CellFB"
  let user : String = (UserDefaults.standard.value(forKey: "UserToken") as? String)!
  
  @IBOutlet weak var pageListCollection: UICollectionView!
  
  override func viewDidLoad() {
    super.viewDidLoad()

    self.title = "Connect with Facebook"
    // Do any additional setup after loading the view.
    self.navigationItem.setHidesBackButton(false, animated:true);
    self.setNavigationbarToTranspalent()
    
    let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
    loadingNotification.mode = MBProgressHUDMode.indeterminate
    loadingNotification.label.text = "Loading"
    
    ConnectAPI<ErrorReponse>().loadAllSelectPage(self.user, storeID: self.storeID!) { responseObject, error in
      let objectData = responseObject! as! [PageResponse]
      
      self.listPage.forEach { page in
        page.isSelected = objectData.contains(page)
      }
      
      self.pageListCollection.reloadData()
      MBProgressHUD.hide(for: self.view, animated: true)
    }
  }
  
  override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.listPage.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! listFBCollectionViewCell
    let page = listPage[indexPath.row]
    cell.listLabel.text = page.name
    cell.listCheckbox.delegate = self
    cell.listCheckbox.tag = indexPath.row
    
    cell.listCheckbox.isChecked = page.isSelected
    let FBPageID = page.facebookPageId ?? ""
    
    let request = FBSDKGraphRequest.init(graphPath: "/\(FBPageID)/photos", parameters: ["fields": "picture"], httpMethod: "GET")
    let connection = FBSDKGraphRequestConnection()
    connection.add(request) { connect, result, error in
      var jsonResult = result as! Dictionary<String, AnyObject>
      let jsonResult2 = jsonResult["data"] as! [Dictionary<String, AnyObject>]
      
      let imgString = jsonResult2[0]["picture"]!
      cell.listImage.image(url: imgString as! String, placeholderNamed: "ImageDefault")
    }
    connection.start()
    
    return cell
  }
  func collectionView(_ collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize {
      let cellSize = CGSize(width: self.pageListCollection.frame.width-10, height: 50)
      return cellSize
  }
  
  func setNavigationbarToTranspalent() {
    self.navigationController?.isNavigationBarHidden = false
    
    let bar:UINavigationBar! =  self.navigationController?.navigationBar
    
    if (bar == nil) {
      
    }else{
      bar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
      bar.shadowImage = UIImage()
      bar.backgroundColor = UIColor(hex: "#37414d")
//      bar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
    }
    
  }
  
  func didSelectCheckbox(_ checkbox: WOWCheckbox) {
    let page = listPage[checkbox.tag]
    page.isSelected = checkbox.isChecked
  }
  
  @IBAction func savePage(_ sender: Any) {
    let selectedPages = listPage.filter({ $0.isSelected })
    
    //TODO: tranform selectedPages to Array
    var allSelectPages = [[String : AnyObject]]()
    
    selectedPages.forEach { page in
      let  dic = [
        "facebookPageId" : page.facebookPageId,
        "pageAccessToken" : page.pageAccessToken
      ]
      allSelectPages.append(dic as [String : AnyObject])
    }
    
    let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
    loadingNotification.mode = MBProgressHUDMode.indeterminate
    loadingNotification.label.text = "Loading"
    let dic = ["storeId" : storeID ?? "","facebookPageList" : allSelectPages] as [String : Any]
    
    
    ConnectAPI<ErrorReponse>().savePage(user, parameters: dic) { responseData, error in
      
      if error == nil{
        //alert
        MBProgressHUD.hide(for: self.view, animated: true)
        VTAlertView()
          .title("Kaojao")
          .message("Done.")
          .type(.alert)
          .button("OK", action: {
            self.navigationController!.popViewController(animated: true)
          })
          .show(self)
      }else {
        //alert
        debugPrint("error?")
        MBProgressHUD.hide(for: self.view, animated: true)
        VTAlertView()
          .title("Kaojao")
          .message("Cannot Save Pages.")
          .type(.alert)
          .button("OK", action: {
            self.navigationController!.popViewController(animated: true)
          })
          .show(self)
      }
    }
  }
  
  
  @IBAction func cancelfunc(_ sender: Any) {
    self.navigationController!.popViewController(animated: true)
  }
}
