//
//  EditStoreDetailViewController.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 14/8/2560 BE.
//  Copyright © 2560 Tana Chaijamorn. All rights reserved.
//

import UIKit
import WOWCheckbox
import KMPlaceholderTextView
import Kingfisher
import MBProgressHUD
import Alamofire

class EditStoreDetailViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate {

  var storeDetail : ListDataResponse?
  @IBOutlet weak var addressField: UITextField!
  @IBOutlet weak var websiteField: UITextField!
  @IBOutlet weak var pictureButton: UIButton!
  @IBOutlet weak var nameFiled: UITextField!
  @IBOutlet weak var showStatus: WOWCheckbox!
  @IBOutlet weak var welcomeField: KMPlaceholderTextView!
  @IBOutlet weak var telField: UITextField!
  @IBOutlet weak var descriptionfield: UITextField!
  @IBOutlet weak var contact1field: UITextField!
  @IBOutlet weak var contact2field: UITextField!
  @IBOutlet weak var contact3field: UITextField!
  @IBOutlet weak var howtoPayFiled: KMPlaceholderTextView!
  @IBOutlet weak var pictureLabel: UILabel!
  
  let picker = UIImagePickerController()
  let reachability = Reachability()
  
  override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("editStore", comment: "editStore")
        self.picker.delegate = self
        self.setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  func setupUI() {
    // TODO: feed api to set UI
    self.websiteField.placeholder = NSLocalizedString("websiteOrPage", comment: "websiteOrPage")
    self.nameFiled.placeholder = NSLocalizedString("storeName", comment: "storeName")
    self.welcomeField.placeholder = NSLocalizedString("welcomeMessage", comment: "welcomeMessage")
    self.telField.placeholder = NSLocalizedString("phone", comment: "phone")
    self.descriptionfield.placeholder = NSLocalizedString("description", comment: "description")
    self.contact1field.placeholder = NSLocalizedString("contactInfo1", comment: "contactInfo1")
    self.contact2field.placeholder = NSLocalizedString("contactInfo2", comment: "contactInfo2")
    self.contact3field.placeholder = NSLocalizedString("contactInfo3", comment: "contactInfo3")
    self.howtoPayFiled.placeholder = NSLocalizedString("paymentInstructions", comment: "paymentInstructions")
    self.pictureLabel.text = NSLocalizedString("photos", comment: "photos")
    self.setDatainField()
  }
  
  func setDatainField() {
    self.addressField.text = storeDetail?.address
    self.nameFiled.text = storeDetail?.nameStore
    self.welcomeField.text = storeDetail?.welcomeMessage
    self.telField.text = storeDetail?.phone
    self.descriptionfield.text = storeDetail?.description
    self.contact1field.text = storeDetail?.contactInfo1
    self.contact2field.text = storeDetail?.contactInfo2
    self.contact3field.text = storeDetail?.contactInfo3
    self.howtoPayFiled.text = storeDetail?.paymentInstructions
    self.websiteField.text = storeDetail?.website
    if storeDetail?.displayStoreMenu == true {
      self.showStatus.isChecked = true
    }else{
      self.showStatus.isChecked = false
    }
    
    let urlString = storeDetail?.logo ?? ""
    if urlString == "" {
//      self.pictureButton.setBackgroundImage(Image(named: "addImage"), for: .normal)
    }else{
      let url = URL(string: urlString)
      self.pictureButton.setImage(Image(named: ""), for: .normal)
      self.pictureButton.sd_setBackgroundImage(with: url, for: .normal, completed: nil)
    }
//    self.uploadimage()
  }
  
  @IBAction func setimageinButtonFunc(_ sender: Any) {
    VTAlertView()
      .title("Select Image")
      .type(.actionSheet)
      .button("Camera", action: {
        self.callImageController(mode: "camera")
      })
      .button("Library", action: {
        self.callImageController(mode: "library")
      })
      .button("Cancel", action: {
      })
      .show(self)
  }
  @IBAction func saveFunc(_ sender: Any) {
    // TODO: edit profile
    self.view.resignFirstResponder()
    storeDetail?.nameStore = self.nameFiled.text
    storeDetail?.contactInfo1 = self.contact1field.text
    storeDetail?.contactInfo2 = self.contact2field.text
    storeDetail?.contactInfo3 = self.contact3field.text
    storeDetail?.description = self.descriptionfield.text
    if showStatus.isChecked == true {
      storeDetail?.displayStoreMenu = true
      storeDetail?.displayStoreMenuString = "true"
    }else{
      storeDetail?.displayStoreMenu = false
      storeDetail?.displayStoreMenuString = "false"
    }
    storeDetail?.paymentInstructions = self.howtoPayFiled.text
    storeDetail?.phone = self.telField.text
    storeDetail?.welcomeMessage = self.welcomeField.text
    storeDetail?.website = self.websiteField.text
    
    if Reachability.isConnectedToNetwork() == true {
      let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
      loadingNotification.mode = MBProgressHUDMode.indeterminate
      loadingNotification.label.text = "Loading"
      
      kaojaoProvider.request(.updateStoreInfo(object: storeDetail!)) { result in
        switch result {
        case let .success(response):
          do {
            
            self.uploadimage()
          } catch {
            
          }
        case .failure(_):
          self.setAlert(AlertStr: "API updateStoreInfo Error")
          MBProgressHUD.hide(for: self.view, animated: true)
        }
      }
    } else {
      MBProgressHUD.hide(for: self.view, animated: true)
      VTAlertView()
        .title("No Internet Connection")
        .message("Make sure your device is connected to the internet.")
        .type(.alert)
        .button("OK", action: {
          
        })
        .show(self)
    }
    
    
  }
  
  func uploadimage() {
    
      
      let storeIDString = storeDetail?.id?.toString ?? ""
      if self.pictureButton.backgroundImage(for: .normal) == nil {
        MBProgressHUD.hide(for: self.view, animated: true)
        VTAlertView().type(.alert).title("Edit Complete").button("Done", action: {
          self.navigationController!.popViewController(animated: true)
        }).show(self)
      }else{
        kaojaoProvider.request(.createLinkforUploadImageLogo(storeId: storeIDString), completion: { result in
          switch result {
          case let .success(response):
            do {
              
              let linkResponse = try response.mapObject(LinkResponse.self)
              let imageData = UIImageJPEGRepresentation(self.pictureButton.backgroundImage(for: .normal)!, 75)
              let urlString = linkResponse.URLItem
              
              Alamofire.upload(imageData!, to: urlString!, method: HTTPMethod.put, headers: ["x-ms-blob-type": "BlockBlob"]).response { response in
                if response.response?.statusCode == 201 {
                  MBProgressHUD.hide(for: self.view, animated: true)
                  VTAlertView().type(.alert).title("Edit Complete").button("Done", action: {
                    self.navigationController!.popViewController(animated: true)
                  }).show(self)
                  
                }else{
                  VTAlertView().type(.alert).title("Error").message("Can not upload image").button("dismiss", action: {
                    
                  }).show(self)
                }
              }
              
            } catch {
              
            }
          case .failure(_):
            self.setAlert(AlertStr: "API createLinkforUploadImageLogo Error")
            MBProgressHUD.hide(for: self.view, animated: true)
          }
        })
      }
    
  }
  
  @IBAction func cancelFunc(_ sender: Any) {
    self.navigationController!.popViewController(animated: true)
  }
  func callImageController(mode: String) {
    picker.allowsEditing = false
    
    if mode == "camera" {
      picker.sourceType = .camera
      picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
    }else{
      picker.sourceType = .photoLibrary
      picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
    }
    
    present(picker, animated: true, completion: nil)
  }
  
  //MARK: - ImagePicker Delegates
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
    self.pictureButton.imageView?.contentMode = .scaleAspectFit
    self.pictureButton.setBackgroundImage(chosenImage, for: UIControlState.normal)
    dismiss(animated:true, completion: nil)
  }
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    dismiss(animated: true, completion: nil)
  }
  // MARK: - Set UI
  func setAlert(AlertStr:String){
    VTAlertView()
      .title("Kaojao")
      .message(AlertStr)
      .type(.alert)
      .button("Dismiss", action: {
        
      })
      .show(self)
    
    let alertController = UIAlertController(title: "Kaojao", message:
      AlertStr, preferredStyle: UIAlertControllerStyle.alert)
    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
    
    self.present(alertController, animated: true, completion: nil)
  }
}
