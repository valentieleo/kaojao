//
//  DetailInfoViewController.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 10/2/16.
//  Copyright © 2016 Tana Chaijamorn. All rights reserved.
//

import UIKit

class DetailInfoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var typelist:String?
    var dataList = [String : AnyObject]()
    var dataDetail = [AnyObject]()
    var detailInfo = ["0","1","2","3","4","5","6","7"]
    
    @IBOutlet weak var tableDetail: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if typelist! == "Income" {
            self.title = NSLocalizedString("incomeTypeIncome", comment: "incomeTypeIncome")
        }else if typelist! == "Estimate" {
            self.title = NSLocalizedString("estimateTypeEstimate", comment: "estimateTypeEstimate")
        }else if typelist! == "Expenses" {
            self.title = NSLocalizedString("accCategoryExpenses", comment: "accCategoryExpenses")
        }
        self.tableDetail.estimatedRowHeight = 68
        self.tableDetail.rowHeight = UITableViewAutomaticDimension
        
        dataDetail = dataList["items"] as! [AnyObject]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if typelist! == "Income" {
            return dataDetail.count+6
        } else {
            return dataDetail.count+4
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 135.0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0001
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HeaderCell
        headerCell.backgroundColor = UIColor.init(hex: "246BBB")
        
        var titlebox = ""
        let titlePrice = dataList["totalAmount"] as? String
        let currencyTitle = UserDefaults.standard.value(forKey: "Currency") as? String
        titlebox = titlePrice! + " " + currencyTitle!
        headerCell.titlePriceLabel.text = titlebox
        
        headerCell.nameLabel.text = dataList["clientContactName"] as? String
        if typelist! == "Income" {
            headerCell.dateLabel.text = dataList["incomeDate"] as? String
            let status = dataList["incomeStatus"] as? String
            
            headerCell.statusLabel.text = status
        }else if typelist! == "Estimate" {
            headerCell.dateLabel.text = dataList["estimateDate"] as? String
            let status = dataList["estimateStatus"] as? String
            
            headerCell.statusLabel.text = status
        }else if typelist! == "Expenses" {
            
        }
        
        headerCell.clipsToBounds = true
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < dataDetail.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailWithTaxCell", for: indexPath) as! DetailWithTaxCell
            cell.backgroundColor = UIColor.clear
            cell.selectionStyle = .none
            
            cell.titleLabel.text = dataDetail[indexPath.row]["name"] as? String
            cell.priceLabel.text = dataDetail[indexPath.row]["subTotal"] as? String
            
            var titlebox = ""
            let titlePrice = dataDetail[indexPath.row]["sellPrice"] as? NSNumber
            let Pricetext = String.init(format: "%.2f", titlePrice!.floatValue)
            let currencyTitle = UserDefaults.standard.value(forKey: "Currency") as? String
            let amount = dataDetail[indexPath.row]["amount"] as? NSNumber
            let amounttext = String.init(format: "%.2f", amount!.floatValue)
            
            let tax = dataDetail[indexPath.row]["tax"] as! String
            var titletax = ""
            if tax == "0.00"{
                titletax = ""
            } else {
                let tax = Float(tax)!
                titletax = " (+\(tax)%)"
            }
            
            titlebox = Pricetext + " (" + currencyTitle! + ") x " + amounttext + titletax
            cell.calLabel.text = titlebox
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath) as! DetailCell
            cell.backgroundColor = UIColor.clear
            cell.selectionStyle = .none
            if typelist! == "Income" {
                if indexPath.row == dataDetail.count {
                    cell.priceLabel.text = dataList["subTotalAmount"] as? String
                    cell.titleLabel.text = NSLocalizedString("subTotal", comment: "subTotal")
                } else if indexPath.row == dataDetail.count+1 {
                    let price = dataList["discountAmount"] as! NSNumber
                    cell.priceLabel.text = String.init(format: "%.2f", price)
                    cell.titleLabel.text = NSLocalizedString("discount", comment: "discount")
                } else if indexPath.row == dataDetail.count+2 {
                    cell.priceLabel.text = dataList["totalTax"] as? String
                    cell.titleLabel.text = NSLocalizedString("taxTotal", comment: "taxTotal")
                } else if indexPath.row == dataDetail.count+3 {
                    cell.priceLabel.text = dataList["totalAmount"] as? String
                    cell.titleLabel.text = NSLocalizedString("total", comment: "total")
                } else if indexPath.row == dataDetail.count+4 {
                    cell.priceLabel.text = dataList["totalPaidAmount"] as? String
                    cell.titleLabel.text = NSLocalizedString("amountPaid", comment: "amountPaid")
                } else if indexPath.row == dataDetail.count+5 {
                    cell.priceLabel.text = dataList["totalRemainingAmount"] as? String
                    cell.titleLabel.text = NSLocalizedString("remainingAmount", comment: "remainingAmount")
                }
            }else if typelist! == "Estimate" {
                if indexPath.row == dataDetail.count {
                    cell.priceLabel.text = dataList["subTotalAmount"] as? String
                    cell.titleLabel.text = NSLocalizedString("subTotal", comment: "subTotal")
                } else if indexPath.row == dataDetail.count+1 {
                    let price = dataList["discountAmount"] as! NSNumber
                    cell.priceLabel.text = String.init(format: "%.2f", price)
                    cell.titleLabel.text = NSLocalizedString("discount", comment: "discount")
                } else if indexPath.row == dataDetail.count+2 {
                    cell.priceLabel.text = dataList["totalTax"] as? String
                    cell.titleLabel.text = NSLocalizedString("taxTotal", comment: "taxTotal")
                } else if indexPath.row == dataDetail.count+3 {
                    cell.priceLabel.text = dataList["totalAmount"] as? String
                    cell.titleLabel.text = NSLocalizedString("total", comment: "total")
                } else if indexPath.row == dataDetail.count+4 {
                    cell.priceLabel.text = dataList["totalPaidAmount"] as? String
                    cell.titleLabel.text = NSLocalizedString("amountPaid", comment: "amountPaid")
                } else if indexPath.row == dataDetail.count+5 {
                    cell.priceLabel.text = dataList["totalRemainingAmount"] as? String
                    cell.titleLabel.text = NSLocalizedString("remainingAmount", comment: "remainingAmount")
                }
            }else if typelist! == "Expenses" {
                
            }
            
            
            return cell
        }
        
    }

}
