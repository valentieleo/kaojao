//
//  UIPlaceHolderTextView.h
//  MasterAppAGA
//
//  Created by Tana Chaijamorn on 5/13/2559 BE.
//  Copyright © 2559 Allianz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIPlaceHolderTextView : UITextView

@property (nonatomic, retain) IBInspectable NSString *placeholder;
@property (nonatomic, retain) IBInspectable UIColor *placeholderColor;

-(void)textChanged:(NSNotification*)notification;

@end
