//
//  LinkResponse.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 10/9/16.
//  Copyright © 2016 Tana Chaijamorn. All rights reserved.
//

import UIKit
import ObjectMapper

class LinkResponse: Mappable {
    
    var URLItem: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        URLItem <- map["uri"]
    }
}
