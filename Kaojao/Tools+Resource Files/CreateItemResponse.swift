//
//  CreateItemResponse.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 10/3/16.
//  Copyright © 2016 Tana Chaijamorn. All rights reserved.
//

import UIKit
import ObjectMapper

class CreateItemResponse: Mappable {
    
    var idItem: Int?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        idItem <- map["id"]
    }
}
