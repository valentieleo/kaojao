//
//  storeDetailResponse.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 7/8/2560 BE.
//  Copyright © 2560 Tana Chaijamorn. All rights reserved.
//

import UIKit
import ObjectMapper

class storeDetailResponse: Mappable {
  
  var address: String?
  var logo: String?
  var id: Int?
  
  required init?(map: Map){
    
  }
  
  func mapping(map: Map) {
    address <- map["address"]
    logo <- map["logo"]
    id <- map["id"]
  }
  
}
