//
//  VTAlertView.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 10/4/2559 BE.
//  Copyright © 2559 Tana Chaijamorn. All rights reserved.
//

enum typeAlert : String {
    case alert
    case actionSheet
}

class VTAlertView {
    private var title = ""
    private var message = ""
    private var type = ""
    private var buttons = [UIAlertAction]()
    
    func title(_ title: String) -> VTAlertView {
        self.title = title
        return self
    }
    
    func message(_ message: String) -> VTAlertView {
        self.message = message
        return self
    }
    
    func type(_ type: typeAlert) -> VTAlertView {
        self.type = type.rawValue
        return self
    }
    
    func button(_ title: String, action: @escaping () -> ()) -> VTAlertView {
        let alertAction = UIAlertAction(title: title, style: .default) { a in action() }
        self.buttons.append(alertAction)
        return self
    }
    
    func show(_ viewController: UIViewController) {
        
        if self.type == "alert" {
            let alertController = UIAlertController(
                title: self.title,
                message: self.message,
                preferredStyle: .alert)
            
            for alertAction in self.buttons {
                alertController.addAction(alertAction)
            }
            
            viewController.present(alertController, animated: true, completion: nil)
        }else{
            let alertController = UIAlertController(
                title: self.title,
                message: self.message,
                preferredStyle: .actionSheet)
            
            for alertAction in self.buttons {
                alertController.addAction(alertAction)
            }
            
            viewController.present(alertController, animated: true, completion: nil)
        }
    }
}
