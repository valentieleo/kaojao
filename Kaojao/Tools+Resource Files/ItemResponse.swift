//
//  itemResponse.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 7/8/2560 BE.
//  Copyright © 2560 Tana Chaijamorn. All rights reserved.
//

import UIKit
import ObjectMapper

class ItemResponse: Mappable {

  var name: String? = ""
  var userId: String? = ""
  var id: Int = 0
  var sellPrice: Float = 0.00
  
  required init?(map: Map){
    
  }
  
  func mapping(map: Map) {
    name <- map["name"]
    userId <- map["userId"]
    id <- map["id"]
    sellPrice <- map["sellPrice"]
  }
}
