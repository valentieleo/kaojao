//
//  ListStoreResponse.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 14/7/2560 BE.
//  Copyright © 2560 Tana Chaijamorn. All rights reserved.
//

import UIKit
import ObjectMapper

class ListStoreResponse: Mappable {

  var list: [PageResponse] = []
  
  required init?(map: Map){
    
  }
  
  func mapping(map: Map) {
    list <- map["data"]
  }
  
}
