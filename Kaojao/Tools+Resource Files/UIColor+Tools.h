//
//  UIColor+Tools.h
//  mainBoardApplication
//
//  Created by Lars Hoss on 22/04/14.
//  Copyright (c) 2014 Allianz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIColor (Tools)

/**
 *  <#Description#>
 *
 *  @param color <#color description#>
 *
 *  @return <#return value description#>
 */
+ (UIColor *)colorWithRGBHex:(NSInteger)color;

/**
 *  <#Description#>
 *
 *  @param hex <#hex description#>
 *
 *  @return <#return value description#>
 */
+ (UIColor *)colorWithHex:(NSString *)hex;

/**
 *  <#Description#>
 *
 *  @param hex   <#hex description#>
 *  @param alpha <#alpha description#>
 *
 *  @return <#return value description#>
 */
+ (UIColor *)colorWithHex:(NSString *)hex andAlpha:(CGFloat)alpha;

@end
