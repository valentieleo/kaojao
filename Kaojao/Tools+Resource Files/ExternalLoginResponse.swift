//
//  FBLoginResponse.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 9/29/2559 BE.
//  Copyright © 2559 Tana Chaijamorn. All rights reserved.
//

import UIKit
import ObjectMapper

class ExternalLoginResponse: Mappable {
    
    var access_token: String?
    var token_type: String?
    var expires: Date?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        access_token <- map["access_token"]
        token_type <- map["token_type"]
        var expires_in: Double = 0
        var expires_string: String = "0"
        expires_string <- map["expires_in"]
        expires_in = expires_string.toDouble()!
        expires = Date().addingTimeInterval(expires_in)
    }
}
