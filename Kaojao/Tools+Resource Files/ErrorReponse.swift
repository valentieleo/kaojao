//
//  ErrorReponse.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 10/23/16.
//  Copyright © 2016 Tana Chaijamorn. All rights reserved.
//

import UIKit
import ObjectMapper

class ErrorReponse: Mappable {
    var message: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        message <- map["message"]
    }
}
