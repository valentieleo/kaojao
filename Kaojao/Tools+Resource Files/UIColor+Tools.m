//
//  UIColor+Tools.m
//  mainBoardApplication
//
//  Created by Lars Hoss on 22/04/14.
//  Copyright (c) 2014 Allianz. All rights reserved.
//

#import "UIColor+Tools.h"

@implementation UIColor (Tools)

+ (UIColor *)colorWithRGBHex:(NSInteger)color {
	return [UIColor colorWithRed:((float)((color & 0xFF0000) >> 16)) / 255.0
	                       green:((float)((color & 0xFF00) >> 8)) / 255.0
	                        blue:((float)(color & 0xFF)) / 255.0
	                       alpha:1.0];
}

+ (UIColor *)colorWithHex:(NSString *)hex {
	return [UIColor colorWithHex:hex andAlpha:1.0];
}

+ (UIColor *)colorWithHex:(NSString *)hex andAlpha:(CGFloat)alpha {
	hex = [[hex stringByReplacingOccurrencesOfString:@"#" withString:@""] uppercaseString];
    
	while ([hex length] < 6) {
		hex = [NSString stringWithFormat:@"0%@", hex];
	}
    
    //	NSLog(@"#3%@", hex);
	// Red Value
	NSString *redHexString = [hex substringWithRange:NSMakeRange(0, 2)];
	NSScanner *redScanner = [NSScanner scannerWithString:redHexString];
	unsigned int redHexInt = 0;
	[redScanner scanHexInt:&redHexInt];
	CGFloat redValue = redHexInt / 255.0f;
    
	// Green Value
	NSString *greenHexString = [hex substringWithRange:NSMakeRange(2, 2)];
	NSScanner *greenScanner = [NSScanner scannerWithString:greenHexString];
	unsigned int greenHexInt = 0;
	[greenScanner scanHexInt:&greenHexInt];
	CGFloat greenValue = greenHexInt / 255.0f;
    
	// Blue Value
	NSString *blueHexString = [hex substringWithRange:NSMakeRange(4, 2)];
	NSScanner *blueScanner = [NSScanner scannerWithString:blueHexString];
	unsigned int blueHexInt = 0;
	[blueScanner scanHexInt:&blueHexInt];
	CGFloat blueValue = blueHexInt / 255.0f;
    //	NSLog(@"#4%f %f %f", redValue, greenValue, blueValue);
    
	return [UIColor colorWithRed:redValue green:greenValue blue:blueValue alpha:alpha];
}
@end
