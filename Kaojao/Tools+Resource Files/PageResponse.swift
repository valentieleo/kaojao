//
//  PageResponse.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 17/7/2560 BE.
//  Copyright © 2560 Tana Chaijamorn. All rights reserved.
//

import UIKit
import ObjectMapper

class PageResponse: Mappable {

  var facebookPageId: String?
  var pageAccessToken: String?
  var name: String?
  var isSelected: Bool = false
  
  required init?(map: Map){
    
  }
  
  func mapping(map: Map) {
    if map.JSON["pageAccessToken"] != nil { // 2. Get Connect with FB ( List )
      facebookPageId <- map["facebookPageId"]
      pageAccessToken <- map["pageAccessToken"]
    } else { // 1. Get Page List with [User Access Token - manage_pages ]
      facebookPageId <- map["id"]
      pageAccessToken <- map["access_token"]
      name <- map["name"]
    }
  }
  
}

extension PageResponse: Equatable {
  static func ==(_ lhs: PageResponse, _ rhs: PageResponse) -> Bool {
    guard lhs.facebookPageId != nil else {
      return false
    }
    return lhs.facebookPageId == rhs.facebookPageId
  }
}
