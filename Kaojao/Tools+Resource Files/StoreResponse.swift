//
//  StoreResponse.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 5/3/2560 BE.
//  Copyright © 2560 Tana Chaijamorn. All rights reserved.
//

import UIKit
import ObjectMapper

class StoreResponse: Mappable {
    var access_token: String?
    var token_type: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        access_token <- map["access_token"]
        token_type <- map["token_type"]
    }
}
