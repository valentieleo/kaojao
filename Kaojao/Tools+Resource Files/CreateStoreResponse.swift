//
//  CreateStoreResponse.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 10/24/16.
//  Copyright © 2016 Tana Chaijamorn. All rights reserved.
//

import UIKit
import ObjectMapper

class CreateStoreResponse: Mappable {
    
    var storeID: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        storeID <- map["id"]
    }
}
