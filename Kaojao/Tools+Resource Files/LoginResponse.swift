//
//  LoginResponse.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 9/28/2559 BE.
//  Copyright © 2559 Tana Chaijamorn. All rights reserved.
//

import UIKit
import ObjectMapper

class LoginResponse: Mappable {

    var userName: String?
    var userEmail: String?
    var access_token: String?
    var token_type: String?
    var expires: Date?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        userName <- map["name"]
        userEmail <- map["email"]
        access_token <- map["access_token"]
        token_type <- map["token_type"]
        var expires_in: Double = 0
        expires_in <- map["expires_in"]
        expires = Date().addingTimeInterval(expires_in)
    }
}
