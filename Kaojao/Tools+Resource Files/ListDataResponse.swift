//
//  ListDataResponse.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 10/1/16.
//  Copyright © 2016 Tana Chaijamorn. All rights reserved.
//

import UIKit
import ObjectMapper

class ListDataResponse: Mappable {
    
    var collection: [ItemResponse] = []
  
    var address: String?
    var logo: String?
    var id: Int?
    var phone: String?
    var contactInfo1: String?
    var contactInfo2: String?
    var contactInfo3: String?
    var description: String?
    var paymentInstructions: String?
    var welcomeMessage: String?
    var nameStore : String?
    var website : String?
    var displayStoreMenu : Bool?
    var displayStoreMenuString : String?
  
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
      
      
      if map.JSON["collection"] != nil {
        collection <- map["collection"]
      } else {
        address <- map["address"]
        logo <- map["logo"]
        id <- map["id"]
        contactInfo1 <- map["contactInfo1"]
        contactInfo2 <- map["contactInfo2"]
        contactInfo3 <- map["contactInfo3"]
        phone <- map["phone"]
        description <- map["description"]
        welcomeMessage <- map["welcomeMessage"]
        paymentInstructions <- map["paymentInstructions"]
        nameStore <- map["name"]
        displayStoreMenu <- map["displayStoreMenu"]
        website <- map["website"]
      }
    }
}
