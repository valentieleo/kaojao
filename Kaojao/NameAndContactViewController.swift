//
//  NameAndContactViewController.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 10/26/16.
//  Copyright © 2016 Tana Chaijamorn. All rights reserved.
//

import UIKit
import MBProgressHUD

class NameAndContactViewController: UIViewController {

    var userCode:String?
    var StoreCode:String?
    var addressCode:String?

    let reachability = Reachability()
    
    @IBOutlet weak var storeNameLabel: UILabel!
    @IBOutlet weak var decriptionTextfield: UITextField!
    @IBOutlet weak var telephoneTextfield: UITextField!
    @IBOutlet weak var contact1Texfield: UITextField!
    @IBOutlet weak var contact2Texfield: UITextField!
    @IBOutlet weak var contact3Texfield: UITextField!
  
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var contactLabel: UILabel!
    @IBOutlet weak var storeNameTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setNavigationbarToTranspalent()
        self.title = NSLocalizedString("signUp", comment: "signUp")
      
      self.storeNameTextfield.placeholderText = NSLocalizedString("storeName", comment: "storeName")
      self.decriptionTextfield.placeholderText = NSLocalizedString("description", comment: "description")
      self.telephoneTextfield.placeholderText = NSLocalizedString("phone", comment: "phone")
      self.contact1Texfield.placeholderText = NSLocalizedString("contactInfo1", comment: "contactInfo1")
      self.contact2Texfield.placeholderText = NSLocalizedString("contactInfo2", comment: "contactInfo2")
      self.contact3Texfield.placeholderText = NSLocalizedString("contactInfo3", comment: "contactInfo3")
    }

    func setNavigationbarToTranspalent() {
        self.navigationController?.isNavigationBarHidden = false
        
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        
        if (bar == nil) {
            
        }else{
            bar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            bar.shadowImage = UIImage()
            bar.backgroundColor = UIColor(hex: "#37414d")
//          bar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func registerfunc(_ sender: AnyObject) {
        if Reachability.isConnectedToNetwork() == true {
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = MBProgressHUDMode.indeterminate
            loadingNotification.label.text = "Loading"
          
            let api = ConnectAPI<ErrorReponse>()
            
            api.storeDetail(userCode!, storeid: StoreCode!, completionHandler: { (response, error) in
                
              api.updateStore(self.userCode!, idStore: self.StoreCode!, addressLinkStore: self.addressCode!, nameStore: self.storeNameTextfield.text!, dataStore: response!, descriptionStore: self.decriptionTextfield.text!, phoneStore: self.telephoneTextfield.text!, contactInfo1: self.contact1Texfield.text!, contactInfo2: self.contact2Texfield.text!, contactInfo3: self.contact3Texfield.text!, completionHandler: { (responsIn, errorIn) in
                    
                    UserDefaults.standard.set(self.userCode!, forKey: "UserToken")
                    
                    self.performSegue(withIdentifier: "toFBLink", sender: nil)
                    
                })
                
            })
            
        } else {
            VTAlertView()
                .title("No Internet Connection")
                .message("Make sure your device is connected to the internet.")
                .type(.alert)
                .button("OK", action: {
                    
                })
                .show(self)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toFBLink" {
          if let PageFBLinkVC = segue.destination as? PageFBLinkViewController {
            PageFBLinkVC.storeID = self.StoreCode
          }
        }
    }
}
