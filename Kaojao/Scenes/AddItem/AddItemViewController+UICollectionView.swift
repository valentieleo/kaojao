//
//  AddItemViewController+UICollectionView.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 26/9/17.
//  Copyright © 2017 Tana Chaijamorn. All rights reserved.
//

extension AddItemViewController: UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    let count = self.assets.count ?? 0
    
    if count <= 3 {
      return count + 1
    }else{
      return count
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! ACImageCollectionViewCell
    
    
    if indexPath.row + 1 <= self.assets.count ?? 0 {
      cell.backgroundColor = UIColor.clear
      cell.textLabel.isHidden = true
      cell.imageData.isHidden = false
      let asset = self.assets[indexPath.row]
      
      let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
      
      asset.fetchImageWithSize(layout.itemSize.toPixel(), completeBlock: { image, info in
        cell.imageData.image = image
        
      })
      
    } else {
      cell.backgroundColor = UIColor.init(hex: "F2F2F2")
      cell.textLabel.text = "+ Photos"
      cell.textLabel.isHidden = false
      cell.imageData.isHidden = true
    }
    
    return cell
  }
}

extension AddItemViewController: UICollectionViewDelegate {
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    view.endEditing(true)
    callImagePicker()
  }
}
extension AddItemViewController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let cellSize = CGSize(width: self.imageCollection.frame.width/2-10, height: self.imageCollection.frame.width/2)
    return cellSize
  }
}
