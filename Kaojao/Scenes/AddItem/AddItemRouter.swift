//
//  AddItemRouter.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 25/9/17.
//  Copyright (c) 2017 Allianz Technology. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

@objc protocol AddItemRoutingLogic {
  
}

protocol AddItemDataPassing {
  var dataStore: AddItemDataStore? { get }
}

class AddItemRouter: NSObject, AddItemRoutingLogic, AddItemDataPassing {
  weak var viewController: AddItemViewController?
  var dataStore: AddItemDataStore?
  
  // MARK: - Route Functions
  
  // MARK: - Navigate Functions
  
  // MARK: - Passing Data Functions
  
}
