//
//  FirstRegisterViewController.swift
//  Kaojao
//
//  Created by MacBook on 22/12/2560 BE.
//  Copyright (c) 2560 Allianz Technology. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import MBProgressHUD

protocol FirstRegisterDisplayLogic: class {

  func displayRegister(viewModel: FirstRegister.signUp.ViewModel)
}

class FirstRegisterViewController: UIViewController, FirstRegisterDisplayLogic {
  var interactor: FirstRegisterBusinessLogic?
  var router: (NSObjectProtocol & FirstRegisterRoutingLogic & FirstRegisterDataPassing)?

  @IBOutlet weak var registerLabel: UILabel!
  @IBOutlet weak var registerButton: UIButton!
  @IBOutlet weak var emailTextfield: UITextField!
  @IBOutlet weak var passwordTextfield: UITextField!
  @IBOutlet weak var comfirmPasswordTextfield: UITextField!
  
  var user:String?
  var storeID:String?
  
  let reachability = Reachability()
  
  // MARK: Object Lifecycle
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  // MARK: - IBAction
  @IBAction func registerfunc(_ sender: AnyObject) {
    let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
    loadingNotification.mode = MBProgressHUDMode.indeterminate
    loadingNotification.label.text = "Loading"
    registerKaojaowith(email: emailTextfield.text!, password: passwordTextfield.text!, rePassword: comfirmPasswordTextfield.text!)
  }
  
  // MARK: View Lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setNavigationbarToTranspalent()
    setLocalize()
  }
  
  // MARK: - Request Functions
  func registerKaojaowith(email: String, password: String, rePassword: String) {
    interactor?.registerKaojao(request: FirstRegister.signUp.Request(email: email, password: password, rePassword: rePassword))
  }
  
  // MARK: - Display Functions
  func displayRegister(viewModel: FirstRegister.signUp.ViewModel) {
    MBProgressHUD.hide(for: self.view, animated: true)
    switch viewModel.status {
    case .email:
      setAlert(AlertStr: "Email is already used.")
    case .gateway:
      setAlert(AlertStr: "Cannot Connect with Backend")
    case .nointernet:
      setAlert(AlertStr: "No Internet Connection")
    case .success:
      print("aaaa")
//      router?.goHome()
    }
  }
  
  // MARK: - Set UI
  func setNavigationbarToTranspalent() {
    self.navigationController?.isNavigationBarHidden = false
    
    let bar:UINavigationBar! =  self.navigationController?.navigationBar
    
    if (bar == nil) {
      
    }else{
      bar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
      bar.shadowImage = UIImage()
      //            bar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
      bar.backgroundColor = UIColor(hex: "#37414d")
    }
  }
  
  func setAlert(AlertStr:String){
    VTAlertView()
      .title("Kaojao")
      .message(AlertStr)
      .type(.alert)
      .button("Dismiss", action: {
        
      })
      .show(self)
    
    let alertController = UIAlertController(title: "Kaojao", message:
      AlertStr, preferredStyle: UIAlertControllerStyle.alert)
    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
    
    self.present(alertController, animated: true, completion: nil)
  }
  
  func setLocalize() {
    self.title = NSLocalizedString("signUp", comment: "signUp")
    self.registerLabel.text = NSLocalizedString("signUp", comment: "signUp")
    self.registerButton.setTitle(NSLocalizedString("signUp", comment: "signUp"), for: UIControlState.normal)
    
    self.comfirmPasswordTextfield.placeholder = NSLocalizedString("confirmPassword", comment: "confirmPassword")
    self.emailTextfield.placeholder = NSLocalizedString("email", comment: "email")
    self.passwordTextfield.placeholder = NSLocalizedString("password", comment: "password")
  }
  
}

// MARK: - Setup, Routing

extension FirstRegisterViewController {
  
  private func setup() {
    let viewController = self
    let interactor = FirstRegisterInteractor()
    let presenter = FirstRegisterPresenter()
    let router = FirstRegisterRouter()
    viewController.interactor = interactor
    viewController.router = router
    interactor.presenter = presenter
    presenter.viewController = viewController
    router.viewController = viewController
    router.dataStore = interactor
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?)
  {
    if let scene = segue.identifier {
      let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
      if let router = router, router.responds(to: selector) {
        router.perform(selector, with: segue)
      }
    }
  }
  
}
