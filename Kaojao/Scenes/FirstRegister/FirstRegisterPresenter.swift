//
//  FirstRegisterPresenter.swift
//  Kaojao
//
//  Created by MacBook on 22/12/2560 BE.
//  Copyright (c) 2560 Allianz Technology. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol FirstRegisterPresentationLogic {
  func presentSignUpKaojao(response: FirstRegister.signUp.Response)
}

class FirstRegisterPresenter: FirstRegisterPresentationLogic {
  weak var viewController: FirstRegisterDisplayLogic?

  // MARK: - Presentation Function
  func presentSignUpKaojao(response: FirstRegister.signUp.Response) {
    
    switch response.result {
    case .success:
      viewController?.displayRegister(viewModel: FirstRegister.signUp.ViewModel(status: .success))
    case .failure(let error):
      switch error {
      case .email:
        viewController?.displayRegister(viewModel: FirstRegister.signUp.ViewModel(status: .email))
      case .noInternet:
        viewController?.displayRegister(viewModel: FirstRegister.signUp.ViewModel(status: .nointernet))
      default:
        viewController?.displayRegister(viewModel: FirstRegister.signUp.ViewModel(status: .gateway))
      }
    }
  }
}
