//
//  ListViewController+UICollectionView.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 25/9/17.
//  Copyright © 2017 Tana Chaijamorn. All rights reserved.
//

import Kingfisher
import SDWebImage

extension ListViewController: UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.list.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let type = router?.dataStore?.type ?? ""
    
    if type == "store" {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! ItemListCollectionViewCell
      let object  = self.list[indexPath.row] as? ListDataResponse
      
      let name : String = object!.address!
      cell.nameLabel.text = "@"+name
      cell.nameLabel.textAlignment = .center
      let urlString = object!.logo ?? ""
      
      let url = URL(string: urlString)
      
      cell.imageView.sd_setImage(with: url, placeholderImage: Image(named: "ImageDefault"))
      
      cell.imageView.backgroundColor = Color.clear
      cell.priceLabel.text = nil
      cell.priceLabel.isHidden = true
      cell.listButton.isHidden = false
      cell.listButton.setCornerRadius(radius: 15.0)
      cell.listButton.tag = object!.id!
      cell.listButton.addTarget(self, action: #selector(pushwithAnimation(animated:)), for: .touchUpInside)
      return cell
    }else{
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! ItemListCollectionViewCell
      
      let object  = self.list[indexPath.row] as? ItemResponse
      cell.nameLabel.text = object?.name
      
      let userId = object?.userId
      let itemId = object?.id
      
      cell.imageView.image = Image(named: "ImageDefault")
      
      if userId == "" || itemId == 0 {
        cell.imageView.image = Image(named: "noImg")
      }else{
        let urlString = "https://acconx.blob.core.windows.net/\(userId!)/items/\(itemId!)/290/1.jpg"
        let url = URL(string: urlString)
        cell.imageView.sd_setImage(with: url, placeholderImage: Image(named: "noImg"))
      }
      let price = String(format: "%.2f ", (object?.sellPrice)!)
      let currencyTitle = UserDefaults.standard.value(forKey: "Currency") as? String
      cell.priceLabel.text = price + currencyTitle!
      return cell
    }
  }
}

extension ListViewController: UICollectionViewDelegate {
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let type = router?.dataStore?.type ?? ""
    if type == "store" {
      //TODO: push to listitemStore
    }
  }
}

extension ListViewController: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let type = router?.dataStore?.type ?? ""
    if type == "store" {
      let cellSize = CGSize(width: self.manuListCollection.frame.width-10, height: self.manuListCollection.frame.height/3)
      return cellSize
    } else {
      let cellSize = CGSize(width: self.manuListCollection.frame.width/2-10, height: self.manuListCollection.frame.width/2)
      return cellSize
    }
  }
}

