//
//  LoginPageRouter.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 5/9/17.
//  Copyright (c) 2017 Allianz Technology. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

@objc protocol LoginPageRoutingLogic {
  func goRight(index: Int)
  func goHome()
}

protocol LoginPageDataPassing {
  var dataStore: LoginPageDataStore? { get }
}

class LoginPageRouter: NSObject, LoginPageRoutingLogic, LoginPageDataPassing {
  
  weak var viewController: LoginPageViewController?
  var dataStore: LoginPageDataStore?
  
  // MARK: - Route Functions
  
  // MARK: - Navigate Functions
  func goHome() {
    let homeVC = viewController?.storyboard?.instantiateViewController(withIdentifier: "HomeView")
    let navigation = UINavigationController(rootViewController: homeVC!)
//    passDataToHome(homeDataStore: homeVC.router.dataStore)
    viewController?.revealViewController().pushFrontViewController(navigation, animated: true)
  }
  
  // MARK: - Passing Data Functions
  func goRight(index: Int) {
    let sendUser = dataStore?.allUsers[index]
    
    let rightMenuViewController = RightMenuViewController()
    rightMenuViewController.router?.dataStore?.userSelect = sendUser!
    
  }
  
//  func passDataToHome(homeDataStore: HomeDataStore) {
//    homeDataStore.user = dataStore!.user!
//  }
}

//class HomeDataStore {
//  var user: LoginResponse
//}

