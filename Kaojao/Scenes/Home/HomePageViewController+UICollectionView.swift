//
//  HomePageViewController+UICollectionView.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 25/9/17.
//  Copyright © 2017 Tana Chaijamorn. All rights reserved.
//

extension HomePageViewController: UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.menulist.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! ManuCollectionViewCell
    
    cell.titleLabel.text = self.menulist[indexPath.row]
    
    if self.menulist[indexPath.row] == NSLocalizedString("chatbots", comment: "chatbots") {
      cell.imageView.image = UIImage(named: "shopping")
    }else if (self.menulist[indexPath.row] == NSLocalizedString("store", comment: "store")) {
      cell.imageView.image = UIImage(named: "store")
    }else if (self.menulist[indexPath.row] == "Messenger") {
      cell.imageView.image = UIImage(named: "messenger")
    }else{
      cell.imageView.image = UIImage(named: "items")
    }
    
    return cell
  }
}

extension HomePageViewController: UICollectionViewDelegate {
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
    if indexPath.row == 0 {
//      interactor?.setDataStore(request: HomePage.setType.Request(selectType: "order"))
      interactor?.setDataStore(request: HomePage.setType.Request(selectType: "store"))
      router?.goListItem()
    }else if (indexPath.row == 1){
      interactor?.setDataStore(request: HomePage.setType.Request(selectType: ""))
//      interactor?.setDataStore(request: HomePage.setType.Request(selectType: "store"))
      router?.goListItem()
    }else if (indexPath.row == 2){
      interactor?.setDataStore(request: HomePage.setType.Request(selectType: ""))
      router?.goListItem()
    }else if (indexPath.row == 3){
      UIApplication.shared.openURL(URL(string: "http://m.me")!)
    }
    
  }
}
extension HomePageViewController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let cellSize = CGSize(width: self.manuListCollection.frame.width/2-10, height: self.manuListCollection.frame.width/2)
    return cellSize
  }
}
