//
//  AppDelegate.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 9/26/2559 BE.
//  Copyright © 2559 Tana Chaijamorn. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

import FBSDKCoreKit

import Firebase

import IQKeyboardManagerSwift
import SWRevealViewController

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
  
  var window: UIWindow?
  
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    Fabric.with([Crashlytics.self])
    
    // Use Firebase library to configure APIs
    FirebaseApp.configure()
    
    IQKeyboardManager.sharedManager().enable = true
    
    GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
    GIDSignIn.sharedInstance().delegate = self
    
    UINavigationBar.appearance().tintColor = UIColor.white
    
    UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
    
    
    if(UserDefaults.standard.value(forKey: "Currency") == nil){
      UserDefaults.standard.set("THB", forKey: "Currency")
    }
    FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    
    
    let dateToken = UserDefaults.standard.value(forKey: "TokenDate") as? Date ?? Date()
    let dateNow = Date()    
    self.window = UIWindow(frame: UIScreen.main.bounds)
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    if dateNow == dateToken {
      //Login Screen
      let loginViewController = storyboard.instantiateViewController(withIdentifier: "LoginView")
      let rightMenuViewController = storyboard.instantiateViewController(withIdentifier: "RightView")
      let loginNavigation = UINavigationController(rootViewController: loginViewController)
      let mainViewController = SWRevealViewController()
      mainViewController.rightViewController = rightMenuViewController
      mainViewController.frontViewController = loginNavigation
      
      self.window?.rootViewController = mainViewController
      self.window?.makeKeyAndVisible()
    }else if (dateNow < dateToken){
      //Home Screen
      let homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeView")
      let rightMenuViewController = storyboard.instantiateViewController(withIdentifier: "RightView")
      let homeNavigation = UINavigationController(rootViewController: homeViewController)
      let mainViewController = SWRevealViewController()
      mainViewController.rightViewController = rightMenuViewController
      mainViewController.frontViewController = homeNavigation
      
      self.window?.rootViewController = mainViewController
      self.window?.makeKeyAndVisible()
    }else{
      //Login Screen
      let loginViewController = storyboard.instantiateViewController(withIdentifier: "LoginView")
      let rightMenuViewController = storyboard.instantiateViewController(withIdentifier: "RightView")
      let loginNavigation = UINavigationController(rootViewController: loginViewController)
      let mainViewController = SWRevealViewController()
      mainViewController.rightViewController = rightMenuViewController
      mainViewController.frontViewController = loginNavigation
      
      self.window?.rootViewController = mainViewController
      self.window?.makeKeyAndVisible()
    }
    
    return true
  }
  
  func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
  }
  
  func applicationDidEnterBackground(_ application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
  }
  
  func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
  }
  
  func applicationDidBecomeActive(_ application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    FBSDKAppEvents.activateApp()
  }
  
  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  }
  
  private func application(_ application: UIApplication, openURL url: URL, sourceApplication: String?, annotation: AnyObject) -> Bool {
    
    
    var urlString = url.absoluteString
    let index = urlString.index(urlString.startIndex, offsetBy: 2)
    urlString = urlString.substring(to: index)
    
    if urlString == "fb" {
      return FBSDKApplicationDelegate.sharedInstance().application( application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }else{
      return GIDSignIn.sharedInstance().handle(url,
                                               sourceApplication: sourceApplication,
                                               annotation: annotation)
    }
    
  }
  
  public func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
    
    var urlString = url.absoluteString
    let index = urlString.index(urlString.startIndex, offsetBy: 2)
    urlString = urlString.substring(to: index)
    
    if urlString == "fb" {
      return FBSDKApplicationDelegate.sharedInstance().application(
        application,
        open: url as URL!,
        sourceApplication: sourceApplication,
        annotation: annotation)
    } else {
      return GIDSignIn.sharedInstance().handle(url,
                                               sourceApplication: sourceApplication,
                                               annotation: annotation)
    }
  }
  
  func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
    if let error = error {
      print(error.localizedDescription)
      return
    }
  }
}

