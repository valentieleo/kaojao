//
//  ListItemStoreViewController.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 5/4/2560 BE.
//  Copyright © 2560 Tana Chaijamorn. All rights reserved.
//

import UIKit
import MBProgressHUD
import Kingfisher

class ListItemStoreViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    var itemStorelist = [ItemResponse]()
    private let reuseIdentifier = "Cell2"
    var storeDetail : ListDataResponse?
    @IBOutlet weak var itemListCollection: UICollectionView!
    let user : String = (UserDefaults.standard.value(forKey: "UserToken") as? String)!
    @IBOutlet weak var imageStore: UIImageView!
    @IBOutlet weak var nameStore: UILabel!
    @IBOutlet weak var editButton: UIButton!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(false, animated:true);
        self.setNavigationbarToTranspalent()
        
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"
        self.editButton.setTitle(NSLocalizedString("editStore", comment: "editStore"), for: UIControlState.normal)
//        self.title = NSLocalizedString("Orders", comment: "Orders")
      
      kaojaoProvider.request(.loadItemInStore(storeName: (storeDetail?.address!)!)) { result in
        switch result {
        case let .success(response):
          do {
            
            let collection = try response.mapObject(ListDataResponse.self)
            
            self.itemStorelist = collection.collection
            
            MBProgressHUD.hide(for: self.view, animated: true)
            self.itemListCollection.reloadData()
          } catch {
            
          }
        case .failure(_):
          self.setAlert(AlertStr: "API loadItem In Store Error")
          MBProgressHUD.hide(for: self.view, animated: true)
        }
      }
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.itemStorelist.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! ManuCollectionViewCell
        
        let object = self.itemStorelist[indexPath.row]
        let currencyTitle = UserDefaults.standard.value(forKey: "Currency") as? String
        let price = String(format: "%.2f", object.sellPrice)

        var titleText = object.name ?? ""
        titleText = titleText + "\n" + price + currencyTitle!
        cell.titleLabel.text = titleText

        let userId = object.userId ?? ""
        let itemId = object.id

        let urlString = "https://acconx.blob.core.windows.net/\(userId)/items/\(itemId)/290/1.jpg"
        let url = URL(string: urlString)
        cell.imageView.sd_setImage(with: url, placeholderImage: Image(named: "noImg"))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        var reusableview = UICollectionReusableView()
        if kind == UICollectionElementKindSectionHeader {
            let headerview = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCell", for: indexPath) as! headerCollectionReusableView
            
            
          var name = storeDetail?.address ?? ""
            name = "@"+name
            headerview.nameHeader.text = name
            
          let urlString = storeDetail?.logo ?? ""
            if urlString == "" {
                headerview.imageHeader.image = Image(named: "ImageDefault")
            }else{
                let url = URL(string: urlString)
                headerview.imageHeader.kf.setImage(with: url)
            }
            reusableview = headerview
        }
        
        return reusableview
    }
    
    func collectionView(_ collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize {
        let cellSize = CGSize(width: self.itemListCollection.frame.width/2-10, height: 250)
        return cellSize
    }
    func setNavigationbarToTranspalent() {
        self.navigationController?.isNavigationBarHidden = false
        
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        
        if (bar == nil) {
            
        }else{
            bar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            bar.shadowImage = UIImage()
//            bar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
          bar.backgroundColor = UIColor(hex: "#37414d")
        }
        
    }
  
    // MARK: - Set UI
    func setAlert(AlertStr:String){
      VTAlertView()
        .title("Kaojao")
        .message(AlertStr)
        .type(.alert)
        .button("Dismiss", action: {
          
        })
        .show(self)
      
      let alertController = UIAlertController(title: "Kaojao", message:
        AlertStr, preferredStyle: UIAlertControllerStyle.alert)
      alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
      
      self.present(alertController, animated: true, completion: nil)
    }
  @IBAction func editButtonFunc(_ sender: Any) {
    let editStoreVC = self.storyboard?.instantiateViewController(withIdentifier: "EditStoreDetailView") as? EditStoreDetailViewController
    editStoreVC?.storeDetail = self.storeDetail
    self.navigationController?.pushViewController(editStoreVC!, animated: true)
  }
}
