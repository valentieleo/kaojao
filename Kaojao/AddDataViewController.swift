//
//  AddDataViewController.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 10/3/16.
//  Copyright © 2016 Tana Chaijamorn. All rights reserved.
//

import UIKit
import MBProgressHUD
import DKImagePickerController
import Alamofire

class AddDataViewController: UIViewController, UITextFieldDelegate, UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet var textView : UIPlaceHolderTextView?
    @IBOutlet weak var savebtn: UIButton!
    @IBOutlet weak var nameItem: UITextView!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var valuePrice: UITextField!
    @IBOutlet weak var widthLabel: NSLayoutConstraint!
    @IBOutlet weak var cancelbtn: UIButton!
    @IBOutlet weak var imageCollection: UICollectionView!
    
    var assets: [DKAsset]?
    let reachability = Reachability()
    
    let currencyTitle = UserDefaults.standard.value(forKey: "Currency") as? String
    private let reuseIdentifier = "ACImageCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setNavigationbarToTranspalent()
        
        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("newItem", comment: "newItem")
        textView?.placeholder = NSLocalizedString("itemName", comment: "itemName")
        currencyLabel.text = currencyTitle
        priceLabel.text = NSLocalizedString("price", comment: "price")
        savebtn.setTitle(NSLocalizedString("save", comment: "save"), for: UIControlState.normal)
        cancelbtn.setTitle(NSLocalizedString("cancel", comment: "cancel"), for: UIControlState.normal)
        valuePrice.placeholder = "0.00"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyboards() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    func setNavigationbarToTranspalent() {
        self.navigationController?.isNavigationBarHidden = false
        
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        
        if (bar == nil) {
            
        }else{
            bar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            bar.shadowImage = UIImage()
//            bar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
          bar.backgroundColor = UIColor(hex: "#37414d")
        }
        
    }

    // MARK: - IBAction
    @IBAction func passOnSaveBtn(_ sender: AnyObject) {
        self.saveItem()
    }
    
    @IBAction func passOnCancelBtn(_ sender: AnyObject) {
        self.navigationController!.popViewController(animated: true)
    }
    
    @IBAction func ChangeText(_ sender: AnyObject) {
        widthLabel.constant = valuePrice.intrinsicContentSize.width
        valuePrice.layoutIfNeeded()
    }
    
    // MARK: - textFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.valuePrice.resignFirstResponder()
        return true
    }
    
    // MARK: - API Upload Image
    
    func saveItem(){
        var token = UserDefaults.standard.value(forKey: "UserToken") as? String
        
        if (token == nil) {
            token = ""
        }
        let name = textView!.text!
        var price = valuePrice!.text!
        if price == "" {
            price = String(0)
        }
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"
      
        ConnectAPI<CreateItemResponse>.createItem(token!, nameString: name, sellInt: Int(price)!, completionHandler: { responseObject, error in
            let IDInt = responseObject?.idItem!
            let itemID = String(IDInt!)
            
            let arrayCount = self.assets?.count
            
            if arrayCount == nil {
                MBProgressHUD.hide(for: self.view, animated: true)
                self.navigationController!.popViewController(animated: true)
            }else{
                for i in 0 ..< self.assets!.count {
                    let asset = self.assets![i]
                    ConnectAPI<LinkResponse>.createLinkforUploadImage(itemID, ItemCount: i+1, completionHandler: { resObj, error in
                        asset.fetchOriginalImage(true, completeBlock: { (image, info) in
                            
                            let imageData = UIImageJPEGRepresentation(image!, 75)
                            let IDInt = resObj?.URLItem!
                            
                            Alamofire.upload(imageData!, to: IDInt!, method: HTTPMethod.put, headers: ["x-ms-blob-type": "BlockBlob"])
                                .response { response in
                                    if i == self.assets!.count-1 {
                                        print(itemID)
                                        
                                        let headers = [
                                            "Authorization": token!,
                                            "Accept": "application/json"
                                        ]
                                        let url :String = String.init(format: "https://api.kaojao.com/api/storage/createthumbnail?itemid=%@", itemID)
                                        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                                            .response(completionHandler: { result in
                                                if result.response?.statusCode == 200 {
                                                    MBProgressHUD.hide(for: self.view, animated: true)
                                                    self.popback()
                                                }else{
                                                    MBProgressHUD.hide(for: self.view, animated: true)
                                                }
                                            })
                                        
                                        
                                    }
                            }
                        })
                    })
                }
            }
            
        })
        
    }
    
    func callImagePicker() {
        let pickerController = DKImagePickerController()
        
        pickerController.defaultSelectedAssets = self.assets
        
        pickerController.maxSelectableCount = 4
        pickerController.didSelectAssets = { (assets: [DKAsset]) in
            self.assets = assets
            self.imageCollection.reloadData()
        }
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.black]
        UINavigationBar.appearance().tintColor = nil
        self.present(pickerController, animated: true) {}
    }
    
    // MARK: - Collection Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count = self.assets?.count ?? 0
        
        if count <= 3 {
            return count + 1
        }else{
            return count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! ACImageCollectionViewCell
        
        
        if indexPath.row + 1 <= self.assets?.count ?? 0 {
            cell.backgroundColor = UIColor.clear
            cell.textLabel.isHidden = true
            cell.imageData.isHidden = false
            let asset = self.assets![indexPath.row]
            
            let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            
            asset.fetchImageWithSize(layout.itemSize.toPixel(), completeBlock: { image, info in
                cell.imageData.image = image
                
            })
            
        } else {
            cell.backgroundColor = UIColor.init(hex: "F2F2F2")
            cell.textLabel.text = "+ Photos"
            cell.textLabel.isHidden = false
            cell.imageData.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.dismissKeyboards()
        self.callImagePicker()
    }
    
    func collectionView(_ collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize {
        let cellSize = CGSize(width: self.imageCollection.frame.width/2-10, height: self.imageCollection.frame.width/2)
        return cellSize
    }
    
    func popback() {
        self.navigationController!.popViewController(animated: true)
    }
  
    // MARK: - Set UI
    func setAlert(AlertStr:String){
      VTAlertView()
        .title("Kaojao")
        .message(AlertStr)
        .type(.alert)
        .button("Dismiss", action: {
          
        })
        .show(self)
      
      let alertController = UIAlertController(title: "Kaojao", message:
        AlertStr, preferredStyle: UIAlertControllerStyle.alert)
      alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
      
      self.present(alertController, animated: true, completion: nil)
    }
}
