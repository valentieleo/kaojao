//
//  ACImageCollectionViewCell.swift
//  Acconx
//
//  Created by Tana Chaijamorn on 8/16/2559 BE.
//  Copyright © 2559 Tana Chaijamorn. All rights reserved.
//

import UIKit

class ACImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageData: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
}
