//
//  ItemListCollectionViewCell.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 3/6/2560 BE.
//  Copyright © 2560 Tana Chaijamorn. All rights reserved.
//

import UIKit

class ItemListCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var listButton: UIButton!
}
