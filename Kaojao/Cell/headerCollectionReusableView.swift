//
//  headerCollectionReusableView.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 5/4/2560 BE.
//  Copyright © 2560 Tana Chaijamorn. All rights reserved.
//

import UIKit

class headerCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var imageHeader: UIImageView!
    @IBOutlet weak var nameHeader: UILabel!
    
    func setLayout() {
        print("done")
    }
    
    var imageHeaderImage: UIImage? {
        didSet {
            imageHeader.image = imageHeaderImage
        }
    }
    
    var nameHeaderText: String? {
        didSet {
            nameHeader.text = nameHeaderText
        }
    }
}
