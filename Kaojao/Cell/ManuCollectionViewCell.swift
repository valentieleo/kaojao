//
//  ManuCollectionViewCell.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 3/2/2560 BE.
//  Copyright © 2560 Tana Chaijamorn. All rights reserved.
//

import UIKit

class ManuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}
