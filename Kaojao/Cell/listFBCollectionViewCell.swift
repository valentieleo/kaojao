//
//  listFBCollectionViewCell.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 17/7/2560 BE.
//  Copyright © 2560 Tana Chaijamorn. All rights reserved.
//

import UIKit
import WOWCheckbox

class listFBCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak var listCheckbox: WOWCheckbox!
  @IBOutlet weak var listLabel: UILabel!
  @IBOutlet weak var listImage: UIImageView!
  
}
