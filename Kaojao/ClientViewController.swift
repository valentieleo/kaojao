//
//  ClientViewController.swift
//  Kaojao
//
//  Created by Tana Chaijamorn on 10/2/16.
//  Copyright © 2016 Tana Chaijamorn. All rights reserved.
//

import UIKit
import CCBottomRefreshControl
import MBProgressHUD
import CGLAlphabetizer

class ClientViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var dataList = [AnyObject]()
    var dic = [String : AnyObject]()
    var titleIndex = [String]()
    var refreshControl: UIRefreshControl!
    var pages : Int = 1
    let user : String = (UserDefaults.standard.value(forKey: "UserToken") as? String)!
    
    @IBOutlet var menuButton:UIBarButtonItem!
    @IBOutlet weak var tableClient: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("clients", comment: "clients")
        // Do any additional setup after loading the view.
        
        self.navigationItem.setRightBarButton(menuButton, animated: true)
        
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"
        
        self.menuButton.image = UIImage(named: "")
        self.menuButton.isEnabled = false
        
        ConnectAPI<ListDataResponse>.loadData("Clients", user: self.user, page: self.pages) { responseObject, error in
            
            self.dataList = (responseObject?.collection)!
            self.dic = CGLAlphabetizer.alphabetizedDictionary(from: self.dataList, usingKeyPath: "contactName") as! [String : AnyObject]
            self.titleIndex = CGLAlphabetizer.indexTitles(fromAlphabetizedDictionary: self.dic) as! [String]
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            // Do any additional setup after loading the view.
            self.tableClient.delegate = self
            self.tableClient.dataSource = self
            self.tableClient.estimatedRowHeight = 68
            self.tableClient.rowHeight = UITableViewAutomaticDimension
            self.tableClient.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.titleIndex.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let title : String = self.titleIndex[section] 
        return self.dic[title]!.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.titleIndex[section]
    }
    
    internal func sectionIndexTitles(for tableView: UITableView) -> [String]?{
        return self.titleIndex
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClientAndItemsCell", for: indexPath) as! ClientAndItemsCell
        if (indexPath.row % 2) != 0 {
            cell.backgroundColor = UIColor.init(hex: "F7F7F7")
        }else{
            cell.backgroundColor = UIColor.white
        }
        
        cell.selectionStyle = .none
        cell.priceLabel.text = ""
        
        let client = self.ACobjectAtIndexPath(indexPath as NSIndexPath)
        var objectIndex = [AnyObject]()
        objectIndex = dic[client]! as! Array
        
        cell.itemLabel.text = objectIndex[indexPath.row]["contactName"] as? String
        
        return cell
        
    }
    
    func ACobjectAtIndexPath(_ indexPath: NSIndexPath) -> String {
        return titleIndex[indexPath.section]
    }

}
